close all
clc
clear
% Load data 
tic

Data = LoadMat_SHFE_CU_Main(80);

price = Data.Ask;


n = length(price);


intervals=[];

a = 1;

for i = 1 : n -1
 
    if price(i+1) == price(i);
        
        a = a + 1;        
        
    else
        
        intervals = [intervals ; a ] ;
        
        a = 1;
        
    end
    
end

if price(end) == price(end-1)
    intervals = [intervals;a];
else
    intervals = [intervals;1];
end


toc

tabulate(intervals)

