function [ Res, FVal, Signal, PnL, NbTrade ] = OptimiseSignal( Fcn, X, Price, Guess, OtherParams, LB, UB, IntCon, Commission, FValType, Vectorized )
%STRATEGYOPTIMISE Summary of this function goes here
%   Detailed explanation goes here

if nargin < 9; Commission = 0; end
if nargin < 10; FValType = 1; end
if nargin < 11; Vectorized = false; end

CostFcn = @(Input)SignalCostFunction(Fcn, X, Price, Input, OtherParams, Commission, FValType);

if isempty(IntCon)
    Options = optimoptions(@fmincon, 'UseParallel', true, 'Display', 'iter');
    Res = fmincon(CostFcn, Guess, [], [], [], [], LB, UB, [], Options);
else
    Options = gaoptimset('UseParallel', true, 'StallGenLimit', 5, 'Display', 'iter'); 
    if Vectorized
        Options = gaoptimset(Options, 'Vectorized', 'on');
    end
    Res = ga(CostFcn, length(Guess), [], [], [], [], LB, UB, [], IntCon, Options);
end

[ FVal, Signal, PnL, NbTrade ] = SignalCostFunction(Fcn, X, Price, Res, OtherParams, Commission, FValType);

end

