function [ FVal, Signal, PnL, NbTrade ] = SignalCostFunction( Fcn, X, Price, Params, OtherParams, Commission, FValType )
%STRATEGYCOSTFUNCTION Summary of this function goes here
%   Detailed explanation goes here

PDiff = Price(2 : end) - Price(1 : end - 1);

Signal = Fcn(X, Params, OtherParams);
NbTrade = sum(abs((Signal(2 : end - 1, :) - Signal(1 : end - 2, :))), 1) + abs(Signal(end - 1, :));
PnL = sum(PDiff' * Signal(1 : end - 1, :), 1) - NbTrade * Commission;

switch FValType
    case 1  % Total PnL
        FVal = PnL;
    case 2  % average PnL per amount
        FVal = PnL ./ NbTrade;
    otherwise
        error('unknown StrategyCostFunction result type');
end
    
FVal = -FVal;

end

