#!/usr/bin/env python

import sys, os
from datetime import datetime
import numpy as np
import scipy.io
import pandas as pd
import hdf5storage


def findAllFiles (rootPath):
    for root, dirs, files in os.walk(rootPath):
        for afile in files:
            if afile.endswith(".mat"):
                yield root, afile
    

class MatDataLoader:
    def __init__(self):
        self.frontMonthContract = pd.DataFrame()
    
    #def procFrontMonthContractRaw(self):
    #    df = pd.read_csv('IF0504_0731.csv')
    #    df['Date'] = df['Date'].astype(str)
    #    self.frontMonthContract = df[['InstrumentID', 'Date']].drop_duplicates()

    def scanFrontMonthContract(self, rootPath, productFilter):
        print rootPath
        endOfDayVols = []
        
        for path, filename in findAllFiles (rootPath):
            print path, filename
            filenameNoExt, filenameExt = os.path.splitext(filename)
            filenameParts = filenameNoExt.split('_')
            if len(filenameParts) < 3:
                raise RuntimeError('Failed to process file ' + filename)
                continue
            exchange = filenameParts[0]
            date = filenameParts[1]
            contract = filenameParts[2]
            product = contract[0:2] #first 2 char is product   
            if product != productFilter:
                continue
            mat = self.loadMatRaw (path, filename)
            #print mat
            vol = mat['VolumeAbs']
            #print vol
            if len(vol) > 0:
                endOfDayVol = vol[-1]
                endOfDayVols.append([exchange, date, product, contract, endOfDayVol])
        #print endOfDayVols
        df = pd.DataFrame (endOfDayVols, columns = ['Exchange', 'Date', 'Product', 'Contract', 'EndOfDayVolume'])
        summary = df.groupby(['Exchange', 'Date', 'Product'], sort=True, as_index=False)
        #for name, group in summary:
        #        print '-----------------------------------------'
        #        print group

        frontMonthDfIdx = df.groupby(['Exchange', 'Date', 'Product'], sort=True, as_index=False)['EndOfDayVolume'].idxmax()
        frontMonthDf = df.iloc[frontMonthDfIdx]
        #print frontMonthDf
        return frontMonthDf
        
    def saveFrontMonthContractToCsv(self, frontMonthDf, filename):
        frontMonthDf.to_csv(filename, index=False)
        print 'Front month contract saved to file ' + filename

    def saveFrontMonthContractFileListToMat(self, frontMonthDf, filename):
        fileList = frontMonthDf['Exchange']+'_'+frontMonthDf['Date']+'_'+frontMonthDf['Contract']+'.mat'
        #frontMonthDf.to_csv(filename, index=False)
        print fileList
        mat_dict = {unicode ('Contracts', 'utf8') : fileList.tolist()}
        hdf5storage.savemat(filename, mat_dict, oned_as = 'row')

        print 'Front month contract saved to file ' + filename
    
    def loadFrontMonthContractFromCsv(self, filename):
        df = pd.read_csv(filename, dtype={'Exchange': str, 'Date': str, 'Product': str, 'Contract': str, 'EndOfDayVolume': str})
        self.frontMonthContract = df
        
    def getFrontMonthContract(self, exchange, date, product):
        exchangeCond = self.frontMonthContract.Exchange.values == exchange
        dateCond = self.frontMonthContract.Date.values == date
        productCond = self.frontMonthContract.Product.values == product
        cond = exchangeCond & dateCond & productCond
        res = self.frontMonthContract.Contract.values[cond]
        if len(res) != 1:
            #raise RuntimeError('Failed to find front month contract for ' + exchange + ' ' + date + ' ' + product)
            print 'Failed to find front month contract for ' + exchange + ' ' + date + ' ' + product
            return []
        return res[0]

    def loadMatRaw(self, path, filename):
        pathfile = os.path.join (path, filename)
        print "Loading Mat file", pathfile
        #mat = scipy.io.loadmat (mat_file)
        mat = hdf5storage.loadmat (pathfile)
        #for key, value in mat.iteritems():
        #    if type(value) is np.ndarray:
        #        mat[key] = value.squeeze()
        #print mat
        return mat
        
    def loadMat(self, exchange, date, contract, path):
        mat_filename = exchange + '_' + date + '_' + contract + '.mat'
        return self.loadMatRaw(path, mat_filename)
        
        
    def loadMatFrontMonthContractOfDay (self, exchange, date, product, path):
        contract = self.getFrontMonthContract (exchange, date, product)
        if not contract:
            print 'Not found'
            return []
        print 'Loading', date, "Contract is", contract
        mat = self.loadMat (exchange, date, contract, path)
        return mat
        
    #TODO mid only right now
    def loadMatFrontMonthContractMultiDay (self, exchange, fromDate, toDate, product, path):
        bdays = pd.bdate_range (fromDate, toDate)
        results = []
        for day in bdays:
            date = day.strftime('%Y%m%d')
            mat = self.loadMatFrontMonthContractOfDay (exchange, date, product, path)
            if not mat:
                print 'No data'
                continue
            bid = mat['BidPrice1']
            ask = mat['AskPrice1']
            mid = (bid+ask)/2.0
            results.append (mid)
        return results
        

    def forOneDay (self, exchange, date, product, path, columnName, functor):
        self.loadMatFrontMonthContractOfDay ()
        contract = self.findFrontMonthContract (date)
        print 'Loading', date, "Contract is", contract
        mat = self.loadMat (exchange, date, contract, path)
        data = mat[columnName].squeeze()
        #popt, pcov, perr, psig = self.fitDensity (powerLawFunc, vol)
        ret = functor(data)
        return list(ret)
        
    def forAll (self, exchange, fromDate, toDate, product, path, columnName, functor):
        bdays = pd.bdate_range (fromDate, toDate)
        results = []
        for day in bdays:
            date=day.strftime('%Y%m%d')
            resDay = self.forOneDay (exchange, date, product, path, columnName, functor)
            #print resDay
            results.append ([date, resDay])
        return results

if __name__ == "__main__":
    #d1 = datetime.strptime(sys.argv[1], '%Y%m%d')
    #d2 = datetime.strptime(sys.argv[2], '%Y%m%d')
    path = sys.argv[1]
    filename = sys.argv[2]
    product = sys.argv[3]
    mds = MatDataLoader()
    df = mds.scanFrontMonthContract(path, product);
    mds.saveFrontMonthContractToCsv(df, filename)
    mds.saveFrontMonthContractFileListToMat(df, filename)
    mds.loadFrontMonthContractFromCsv(filename)
    #print mds.getFrontMonthContract('shfe', '20150831', 'cu')

