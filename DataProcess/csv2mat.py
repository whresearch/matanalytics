#!/usr/bin/env python
# -*- coding: utf-8 -*-
#from __future__ import unicode_literals
import sys, os, getopt, errno
import numpy, scipy.io
import pandas as pd
import csv as csv
import hdf5storage

def prependColumns (df):
    df['MidPrice'] = float('NaN')
    df['VolumeAbs'] = 0
    df['OpenInterestAbs'] = 0
    df['OpenPosition'] = 0
    df['ClosePosition'] = 0
    
def processData (input):
    df = input.copy(deep=True)
    #print df.dtypes
    #df['OpenInterest'] = df['OpenInterest'].astype(int)
    df['MidPrice'] = (df['AskPrice1'] + df['BidPrice1'])/2.0
    df['VolumeAbs'] = df['Volume']
    df['Volume'] = df['VolumeAbs'].diff()
    df['OpenInterestAbs'] = df['OpenInterest']
    df['OpenInterest'] = df['OpenInterestAbs'].diff()
    df['OpenPosition'] = (df['Volume'] + df['OpenInterest'])/2
    df['ClosePosition'] = (df['Volume'] - df['OpenInterest'])/2
    return df
    #print df.dtypes

def WangCsv2MatCffex(csv_file, mat_prefix='cffex'):
    df = pd.read_csv(csv_file)
    filtered = df[(df.SecondOfDay >= 33240) & (df.SecondOfDay <= 54900)]
    grouped = filtered.groupby ('InstrumentID')


    for name, group in grouped:
        print(name)
        cols = group.columns.values.tolist()
        cols.remove('InstrumentID')
        cols.remove('UpdateTime')
        processData (group)
        #cols.remove('Time')
        a_dict = {col_name : group[col_name].values for col_name in cols}
        scipy.io.savemat(mat_prefix + '_' + name + '.mat', a_dict, oned_as = 'column')

def makePath(rootPath, exch, yearMonth, product):
    return os.path.join(rootPath, exch, yearMonth, product)

def makeFilename(exch, date, contract):
    return exch+'_'+date+'_'+contract

def productFromContract (contract):
    return contract[:2]

def WangCsv2MatShfe(csv_file, root_path):
    filename = os.path.splitext(os.path.basename(csv_file))[0]
    filenameSplit = filename.split('_')
    exch = filenameSplit[0]
    date = filenameSplit[1]
    yearMonth = date[:6]

    df = pd.read_csv(csv_file)
    prependColumns (df)
    #print df.AskPrice1
    #print type(df.AskPrice1)
    
    filtered = df[(df.SecondOfDay >= 75600) & (df.SecondOfDay <= 140400) & ~numpy.isnan(df.AskPrice1) & ~numpy.isnan(df.BidPrice1)]

    grouped = filtered.groupby ('InstrumentID')


    for contract, group in grouped:
        print(contract)
        product = productFromContract (contract)

        processed = processData (group)

        cols = processed.columns.values.tolist()
        cols.remove('InstrumentID')
        cols.remove('UpdateTime')
        #cols.remove('Time')
        mat_dict = {unicode (col_name, 'utf8') : processed[col_name].values for col_name in cols}
        path = makePath (root_path, exch, yearMonth, product)
        filename = makeFilename (exch, date, contract)
        savemat (path, filename, mat_dict)
         

def savemat(path, filename, mat_dict):
    mkdir_p (path)
    abspathfile = os.path.join (path, filename)
    #print path, filename, mat_dict
    hdf5storage.savemat(abspathfile, mat_dict, oned_as = 'column')
    #scipy.io.savemat(mat_prefix + '_' + name + '.mat', a_dict, oned_as = 'column')

def convert(input):
    if isinstance(input, dict):
        return {convert(key): convert(value) for key, value in input.iteritems()}
    elif isinstance(input, list):
        return [convert(element) for element in input]
    elif isinstance(input, unicode):
        return input.encode('utf-8')
    else:
        return input

def OrderSubmitRecordToMat (csv_file, mat_prefix='order_submit_record'):
    import locale
    locale.setlocale( locale.LC_ALL, 'en_US.UTF-8' ) 
    
    pd.set_option('display.expand_frame_repr', False)
    
    conv={u'委托价': locale.atof}
    
    
    df = pd.read_csv(csv_file,
                     engine = 'python',
                     converters = conv,
                     #sep=str(','),
                     #usecols = ['交易日','投资者代码'],
                     usecols = [u'交易日',u'投资者代码',u'买卖',u'开平',u'委托量',u'委托价',u'委托时间',u'报单价格条件',u'有效期类型',u'成交量类型',u'合约在交易所的代码',u'今成交数量',u'剩余数量',u'报单日期'],
                     skipfooter = 1,
                     #quotechar = '"',
                     #escapechar = '"',
                     #quoting = csv.QUOTE_ALL,
                     encoding='utf-8-sig'
                     )

    df.columns = ['TradingDay','InvestorID','Side','Offset','OrderQty','Price','OrderTime','OrderPriceCondition','TimeInforce','OrderQtyCondition','InstrumentID','FilledQty','LeavesQty','OrderDate']
    
    sideTranslate = {u'买':'buy', u'卖':'sell'}
    offsetTranslate = {u'开仓':'open', u'平今':'close today', u'平昨':'close yesterday'}
    pxCondTranslate = {u'限价':'limit'}
    tifTranslate = {u'当日有效':'day', u'立即完成，否则撤销':'ioc'}
    qtyCondTranslate = {u'任何数量':'any'}
    
    #df=df[['TradingDay','InvestorID','Contract','Side']]
    df['Side'].replace(sideTranslate, inplace=True)
    df['Offset'].replace(offsetTranslate, inplace=True)
    df['OrderPriceCondition'].replace(pxCondTranslate, inplace=True)
    df['TimeInforce'].replace(tifTranslate, inplace=True)
    df['OrderQtyCondition'].replace(qtyCondTranslate, inplace=True)
    #print df.head()
    grouped = df.groupby (['InvestorID', 'TradingDay', 'InstrumentID'], sort=True, as_index=False)
      
    for name, group in grouped:
        print '-----------------------------------------'
        #print group.head()
        (InvestorID, TradingDay, InstrumentID) = name
        cols = group.columns.values.tolist()
        a_dict={}
        for col_name in cols:
            v = group[col_name].values
            #print v, v.dtype
            #print type(v)
            a_dict[col_name] = v
        
        #print a_dict
        filename= '{}_{}_{}_{}.mat'.format(mat_prefix, InvestorID, TradingDay, InstrumentID)
        print filename
        scipy.io.savemat(filename, a_dict, oned_as = 'column')
    

def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc: # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else: raise
 

def main():
    inputfile = ''
    outputpath = ''
    try:
        opts, args = getopt.getopt(sys.argv[1:],"hi:o:",["ifile=","opath="])
    except getopt.GetoptError:
        print sys.argv[0] + ' -i <inputfile> -o <outputpath>'
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print sys.argv[0] + ' -i <inputfile> -o <outputpath>'
            sys.exit()
        elif opt in ("-i", "--ifile"):
            inputfile = arg
        elif opt in ("-o", "--opath"):
            outputpath = arg

    print 'Input file is "', inputfile
    print 'Output path is "', outputpath

    WangCsv2MatShfe(inputfile, outputpath)
    #OrderSubmitRecordToMat (filename)

if __name__ == "__main__":
    main()
    
