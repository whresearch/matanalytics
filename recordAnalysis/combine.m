
d = datetime(datestr, 'InputFormat', 'yyyyMMdd');
h = floor(record.OrderTime(1:2) / 3600);
m = floor(data.SecondOfDay / 60) - h .* 60;
s = double(data.SecondOfDay - h .* 3600 - m .* 60) + double(data.UpdateMillisec) ./ 1000;
dt = d + duration(h - 24, m, s);
