clc
clear

load('table0507.mat');
load('order_submit_record_10301600_20150507_cu1506.mat');

dataTable = table1;
n = size(OrderTime,1);

[ h, OrderTime ] = strtok(OrderTime, ':');
[ m, OrderTime ] = strtok(OrderTime, ':');
[ s, OrderTime ] = strtok(OrderTime, ':');
h = str2double(h);
m = str2double(m);
s = str2double(s);
h = h + (h < 17) * 24;
time = datetime(2015,05,06,h,m,s);

OrderQty = double(OrderQty);
FilledQty = double(FilledQty);
time.Format = 'yyyy-MM-dd hh:mm:ss.SSS';

recTable = table(time, Side, Price, OrderQty, FilledQty, Offset);
recTable = sortrows(recTable);

% compute total position

filledQty = recTable.FilledQty;

side      = recTable.Side;

totalPosition = zeros(n,1);

currentPos = 0;

for i = 1 : n
    
    if strcmp(side{i}, 'buy')
        
        currentPos = currentPos + filledQty(i);    
        
    elseif strcmp(side{i}, 'sell')
        
        currentPos = currentPos - filledQty(i);    
        
    end
    
    totalPosition(i) = currentPos;
    
end

recTable.totalPosition = totalPosition;


%


%compute avgCost and realizedPnL
curAvgCost = 0;
realizedPnL = 0;
tmpPnL=0;
curCost;
if strcmp(offset{1},'open')
    
    curCost = filledQty(1)*Price(1);

elseif strcmp(offset{1},'close today')

    
    
end
    
    
for i = 2 : n
    
    
    if strcmp(offset{i},'open')
        curCost = filledQty(i)*Price(i);
        curAvgCost = curAvgCost * totalPostion 
        
        
    elseif strcmp(offset{i},'close today')
        
    
    
    end
end




time = recTable.time;
timeOffset = zeros(n, 1);
for i = 2 : n
    
    j = 0;
    while time(i - j) == time(i - j - 1)
        timeOffset(i) = timeOffset(i) + 0.001;
        j = j + 1;
        if i - j == 1
            break;
        end
    end
    
end

time = time + seconds(timeOffset);
recTable.time = time;

time = union(dataTable.time, recTable.time);
[ ~, ia ] = intersect(dataTable.time, time);
dataTable = dataTable(ia, :);

dataCells = table2cell(dataTable);
recCells = table2cell(recTable);


iData = 1;
iRec = 1;
combinedCells = cell(length(time), size(dataCells, 2) + size(recCells, 2) - 1);

for i = 1 : length(time)
    
    combinedCells{i, 1} = time(i);
    
    idx = find(dataTable.time == time(i), 1);
    if ~isempty(idx)
        for j = 2 : size(dataCells, 2)
            combinedCells{i, j} = dataCells{idx, j};
        end
    end
    
    idx = find(recTable.time == time(i), 1);
    if ~isempty(idx)
        for j = 2 : size(recCells, 2)
            combinedCells{i, size(dataCells, 2) + j - 1} = recCells{idx, j};
        end
    end
    
end

combinedTable = cell2table(combinedCells, ...
    'VariableNames', {'Time' 'BidSize' 'BidPrice' 'VWAP' 'AskPrice' 'AskSize'...
    'Volume' 'OpenVolume' 'Side' 'Price' 'OrderQty' 'FilledQty' 'Offset'});