clear
clc

%data = LoadMat_SHFE_CU_Main(1);

data = LoadMat_SHFE('shfe_20150507_cu1506.mat');

time        = data.Time;
bidSize     = data.BidSize;
bidPrice    = data.Bid;
vwap        = data.VWAP;
askPrice    = data.Ask;
askSize     = data.AskSize;
vol         = data.Volume;
volOpen     = data.OpenVolume;

table1 = table(time,bidSize,bidPrice,vwap,askPrice,askSize,vol,volOpen);

record = load('order_submit_record_10301600_20150506_cu1506.mat');

clearvars -except table1