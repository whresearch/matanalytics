function [result, cdfBid, cdfAsk] = fillAnalysisOneDay(Data, entryCond, maxFillTime, maxQueuePos, fillTimestep, queuePosStep)

    [fillTimeBid, avgFillTimeBid, numFilledBid, numOrdersBid, filledRatioBid] = ...
        fillAnalysisOneSide (Data.Bid, entryCond, maxFillTime, true);
    [fillTimeAsk, avgFillTimeAsk, numFilledAsk, numOrdersAsk, filledRatioAsk] = ...
        fillAnalysisOneSide (Data.Ask, entryCond, maxFillTime, false);

    
    
    fillTimeBothSide = max (fillTimeBid, fillTimeAsk);
    fillTimeAtLeastOneSide = min (fillTimeBid, fillTimeAsk);
    
    filledCondBothSide = fillTimeBothSide<maxFillTime;
    filledCondAtLeastOneSide = fillTimeAtLeastOneSide<maxFillTime;
    
    fillTimeBidBsf = fillTimeBid(filledCondBothSide);
    fillTimeAskBsf = fillTimeAsk(filledCondBothSide);
    
    fillTimeBidFilled = fillTimeBid(fillTimeBid<maxFillTime);
    fillTimeAskFilled = fillTimeAsk(fillTimeAsk<maxFillTime);
    
    fillTimeAvgDiff = mean(abs (fillTimeBidBsf - fillTimeAskBsf));
    
    if isempty (fillTimeBidBsf) || isempty (fillTimeAskBsf)
        fillTimeCorrelation = NaN;
    else
        fillTimeCorrelation = corr (fillTimeBidBsf, fillTimeAskBsf);
    end
    %test = 1:10;
    
    queuePosBidBsf = Data.BidSize(filledCondBothSide);
    queuePosAskBsf = Data.AskSize(filledCondBothSide);
    
    queuePosBidFilled = Data.BidSize(fillTimeBid<maxFillTime);
    queuePosAskFilled = Data.AskSize(fillTimeAsk<maxFillTime);
    

    
    cdfBid = fillTimeDistribution (fillTimeBid, Data.BidSize, maxFillTime, maxQueuePos, fillTimestep, queuePosStep);
    cdfAsk = fillTimeDistribution (fillTimeAsk, Data.AskSize, maxFillTime, maxQueuePos, fillTimestep, queuePosStep);
    
    %b= [1:5, 1:5]';
    %a= [1:10]';
    %cdf = fillTimeDistribution (a, b, 20, 5);

    %hold off
    %scatter (queuePosBidFilled, fillTimeBidFilled);
    
    queuePosAvgBidBsf = mean (queuePosBidBsf);
    queuePosAvgAskBsf = mean (queuePosAskBsf);
    
    avgFillTimeBothSide = mean (fillTimeBothSide(filledCondBothSide));
    avgFillTimeAtLeastOneSide = mean (fillTimeAtLeastOneSide(filledCondAtLeastOneSide));

    numFilledBothSide = sum (filledCondBothSide);
    numFilledAtLeastOneSide = sum (filledCondAtLeastOneSide);
    numFilledOnlyOneSide = numFilledAtLeastOneSide - numFilledBothSide;
    numOrders = numOrdersBid; % assuming bid and ask are same

    ratioBothSideOverAtLeastOne = numFilledBothSide / numFilledAtLeastOneSide;
    ratioOnlyOneSideOverAtLeastOne = numFilledOnlyOneSide / numFilledAtLeastOneSide;

    ratioAtLeastOneSideOverAll = numFilledAtLeastOneSide / numOrders;

    result = table (numOrders, ...
        numFilledAtLeastOneSide, numFilledOnlyOneSide, numFilledBothSide, ...
        fillTimeAvgDiff, fillTimeCorrelation, ...
        queuePosAvgBidBsf, queuePosAvgAskBsf, ...
        avgFillTimeBid, avgFillTimeAsk, avgFillTimeBothSide, avgFillTimeAtLeastOneSide, ...
        ratioBothSideOverAtLeastOne, ratioOnlyOneSideOverAtLeastOne, ratioAtLeastOneSideOverAll ...
        );
end