function results = fillAnalysisArg (days, maxFillTime, style, maxQueuePos)

    colNames = {'numOrders' ...
        'numFilledAtLeastOneSide' 'numFilledOnlyOneSide' 'numFilledBothSide' ...
        'fillTimeAvgDiff' 'fillTimeCorrelation' ...
        'queuePosAvgBidBsf' 'queuePosAvgAskBsf' ...
        'avgFillTimeBid' 'avgFillTimeAsk' 'avgFillTimeBothSide' 'avgFillTimeAtLeastOneSide' ...
        'ratioBothSideOverAtLeastOne' 'ratioOnlyOneSideOverAtLeastOne' 'ratioAtLeastOneSideOverAll' ...
        };
    placeHolder = cell(0,length(colNames));
    results = cell2table(placeHolder);
    results.Properties.VariableNames = colNames;
    for i = days
        Data = LoadMat_SHFE_CU_Main(i);
        Data.FilterByHours(9, 11.5);
        % Data.FilterByPrice('Mid');
        % Calculate Mid-Price
        %mid = (Data.Bid + Data.Ask) / 2;
        
        if style == 0
            entryCond = true (size(Data.Bid));
        elseif style == 1
            entryCond = (Data.BidSize < maxQueuePos & Data.AskSize < maxQueuePos);
        elseif style == 2
            imbalance = Data.BidSize./(Data.AskSize+Data.BidSize);
            imbalanceCond = imbalance > 0.7 | imbalance < 0.3;
            entryCond = (Data.BidSize < maxQueuePos & Data.AskSize < maxQueuePos & imbalanceCond);
        end
        
        result = fillAnalysisOneDay (Data, entryCond, maxFillTime, maxQueuePos, 1, 1);
        results = [results;result];
    end
end