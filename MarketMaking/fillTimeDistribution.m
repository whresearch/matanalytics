function cdf = fillTimeDistribution (fillTime, queuePos, maxFillTime, maxQueuePos, fillTimeStep, queuePosStep)
    numFillTime = maxFillTime/fillTimeStep;
    numQueuePos = maxQueuePos/queuePosStep;
    N = zeros (numFillTime, numQueuePos);
    edges = 1:numFillTime+1;
    for pos = 1:numQueuePos
        p1 = (pos-1) * queuePosStep + 1;
        p2 = p1 + queuePosStep;
        fills = fillTime(queuePos >=p1 & queuePos < p2);
        pdf = histcounts (fills, edges, 'Normalization','probability');
        N(:,pos) = pdf';
    end
    cdf = cumsum(N);
end