% fill time distribution
% fill time correlation (ask/bid)
% fill time joint distribution (ask/bid)
% fill time conditional on queue position


clc
clear
% Load data 
tic
figure
days = 1:60;
maxFillTime = 200;
maxQueuePos = 200;
results0 = fillAnalysisArg (days, maxFillTime, 0, maxQueuePos);
results1 = fillAnalysisArg (days, maxFillTime, 1, maxQueuePos);
results2 = fillAnalysisArg (days, maxFillTime, 2, maxQueuePos);

toc

hold on
edges = 0:0.1:1;
xbins = 10;
subplot(2,2,1)
hold on
h0 = histogram(results0.ratioBothSideOverAtLeastOne,xbins, 'Normalization','probability', 'FaceColor', 'r');
h1 = histogram(results1.ratioBothSideOverAtLeastOne,xbins, 'Normalization','probability', 'FaceColor', 'g');
h2 = histogram(results2.ratioBothSideOverAtLeastOne,xbins, 'Normalization','probability', 'FaceColor', 'b');
subplot(2,2,2)
hold on
histogram(results0.fillTimeCorrelation, xbins, 'Normalization','probability', 'FaceColor', 'r');
histogram(results1.fillTimeCorrelation, xbins, 'Normalization','probability', 'FaceColor', 'g');
histogram(results2.fillTimeCorrelation, xbins, 'Normalization','probability', 'FaceColor', 'b');
subplot(2,2,3)
hold on
histogram(results0.queuePosAvgBidBsf, xbins, 'Normalization','probability', 'FaceColor', 'r');
histogram(results1.queuePosAvgBidBsf, xbins, 'Normalization','probability', 'FaceColor', 'g');
histogram(results2.queuePosAvgBidBsf, xbins, 'Normalization','probability', 'FaceColor', 'b');
