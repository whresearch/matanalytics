function [F, J] = orderHitCdfFunc (x, xdata)
    F = x(1)*exp(-x(2)*xdata)+x(3);
    J(:, 1) = exp(-x(2)*xdata);
    J(:, 2) = -xdata.*(x(1)*exp(-x(2)*xdata));
    J(:, 3) = 1;
end

