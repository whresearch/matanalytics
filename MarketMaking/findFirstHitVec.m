function firstHit = findFirstHitVec(x, condition, maxFillTime, isBid)
    firstHit = ones(size(x))*maxFillTime;
    for i = 1:(length(x)-maxFillTime)
        if condition(i)
            t = findFirstHit (x(i+1:i+maxFillTime),x(i), isBid);
            if ~isempty(t)
                firstHit(i) = t;
            end
        end
    end
end

