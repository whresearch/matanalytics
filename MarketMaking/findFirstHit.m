function idx = findFirstHit(x, px, isBid)
    if isBid
        cond = x < px;
    else
        cond = x > px;
    end
    idx = find (cond, 1);
end