days=1:60;
numDays = length (days);
maxFillTime = 200;
maxQueuePos = 60;

reduceFactor = 2;
reducedMaxQueuePos = maxQueuePos/reduceFactor;
reducedMaxFillTime = maxFillTime/reduceFactor;

matrix = zeros(57603, numDays);
matrix = zeros(57603, numDays);
allCdfBid = zeros (reducedMaxFillTime,reducedMaxQueuePos,numDays);
allCdfAsk = zeros (reducedMaxFillTime,reducedMaxQueuePos,numDays);

fillProbAtTime = 10; % 20 ticks
fillProbAtTimeBid = zeros (reducedMaxQueuePos, numDays);
fillProbAtTimeAsk = zeros (reducedMaxQueuePos, numDays);

%fun = @(x,xdata)x(1)*exp(-x(2)*xdata)+x(3);
fitParamBid = zeros (numDays, 3);
fitParamAsk = zeros (numDays, 3);

%options = optimoptions('lsqcurvefit', 'Jacobian', 'off', 'TolFun',1e-8, 'TolX', 1e-8, 'Display','off'); % default=1e-6
initParams = [1,1,1];
ub=[100;100;100];
lb=[-100;-100;-100];
for i = days
   Data = LoadMat_SHFE_CU_Main(i);
   matrix(:,i) = Data.Volume;
   ratio = (Data.VWAP - Data.Bid) ./ (Data.Ask - Data.Bid);
   ratio(ratio < 0) = 0;
   ratio(ratio > 1) = 1;
   volAsk(:,i) = ratio .* Data.Volume;
   volBid(:,i) = (1-ratio) .* Data.Volume;
   
   entryCond = true (size(Data.Bid));
   
   [result, cdfBid, cdfAsk] = fillAnalysisOneDay (Data, entryCond, maxFillTime, maxQueuePos, reduceFactor, reduceFactor);
   
   allCdfBid(:,:,i) = cdfBid;
   allCdfAsk(:,:,i) = cdfAsk;
   
   
   fillProbAtTimeBid(:, i) = cdfBid (fillProbAtTime,:);
   fillProbAtTimeAsk(:, i) = cdfAsk (fillProbAtTime,:);
   
   fitParamBid(i,:) = lsqcurvefit (@orderHitCdfFunc, initParams, 1:reducedMaxQueuePos, fillProbAtTimeBid(:,i)');%, lb, ub, options);
   fitParamAsk(i,:) = lsqcurvefit (@orderHitCdfFunc, initParams, 1:reducedMaxQueuePos, fillProbAtTimeAsk(:,i)');%, lb, ub, options);
   
end



nrows = size(matrix, 1);
bucketSize = 2*60;
nbuckets = floor(nrows / bucketSize);
for i = 1:nbuckets
    start = (i-1) * bucketSize +1;
    %start, start+bucketSize-1
    subMat = matrix (start:(start+bucketSize-1), :);
    sumMat(i, :) = sum (subMat, 1);
end