function [fillTime, avgFillTime, numFilled, numOrders, filledRatio] = fillAnalysisOneSide (x, condition, maxFillTime, isBid)
    fillTime= findFirstHitVec (x, condition, maxFillTime,  isBid);
    avgFillTime = mean (fillTime(fillTime<maxFillTime & condition));

    numFilled = sum (fillTime<maxFillTime & condition);
    numOrders = sum (condition);

    filledRatio = numFilled / numOrders;
end

