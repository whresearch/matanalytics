function [ Trend, MC, MR ] = CCRT( Prices, Lookback, MinLag, MaxLag )
%MPRT Summary of this function goes here
%   Detailed explanation goes here

MC = NaN(length(Prices), 1);
MR = NaN(length(Prices), 1);
Trend = NaN(length(Prices), 1);
Direction = zeros(length(Prices), 1);

for i = Lookback + 1 : length(Prices)
    PC = zeros(MaxLag, 1);
    PR = zeros(MaxLag, 1);
    Chg = zeros(MaxLag, 1);
    
    for q = MinLag : MaxLag
        X = Prices(i - Lookback + q : i) - Prices(i - Lookback : i - q);
        for t = q + 1 : Lookback - q + 1
            Signal = X(t) * X(t - q);
            if Signal > 0
                PC(q) = PC(q) + 1;
            else % if Signal < 0
                PR(q) = PR(q) + 1;
            end
        end
        PC(q) = PC(q) / (Lookback - q);
        PR(q) = PR(q) / (Lookback - q);
        Chg(q) = (Prices(i) - Prices(i - q)) / q;
    end
    
    ChgSum = sum(Chg);
    if ChgSum > 0
        Direction(i) = 1;
    elseif ChgSum < 0
        Direction(i) = -1;
    end
    
    MC(i) = sum(PC) / (MaxLag - MinLag);
    MR(i) = sum(PR) / (MaxLag - MinLag);
end
Index = MC > MR;
Trend(Index) = Direction(Index);
Trend(MC <= MR) = 0;

end

