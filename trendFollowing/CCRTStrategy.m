classdef CCRTStrategy < Strategy
    %MKTSTRATEGY Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        Price_Interval = 1;
        MA_Lookback = 5;
        CCRT_Lookback = 40;
        CCRT_MinLag = 10;
        CCRT_MaxLag = 15;
        Signal_Lookbook = 5;
        MaxHoldPeriod = 60;
        StopLoss = -60;
        TakeProfit = 100;
        
        Vals;
        TradePrice;
        HoldPeriod;
        TCount;
        LiveAmount;
    end
    
    methods
        
        function this = CCRTStrategy()
            this.Vals = NaN(this.Price_Interval * (this.CCRT_Lookback + this.MA_Lookback * 10), 1);
            this.TradePrice = 0;
            this.HoldPeriod = 0;
            this.TCount = 0;
            this.LiveAmount = 0;
        end
        
        function OnMarketData( this, Snapshot )
            
            Amount = 0;
            
            this.TCount = this.TCount + 1;
            this.HoldPeriod = this.HoldPeriod + 1;
            
            if this.TCount == this.Price_Interval
                this.Vals(1:end-1) = this.Vals(2:end);
                this.Vals(end) = Snapshot.Mid;
                this.TCount = 0;
            end
            
            if this.LiveAmount > 0
                if Snapshot.Bid(1) - this.TradePrice <= this.StopLoss ...
                || Snapshot.Bid(1) - this.TradePrice >= this.TakeProfit ...
                || this.HoldPeriod == this.MaxHoldPeriod
                    Amount = -this.LiveAmount;
                elseif Snapshot.Bid(1) > this.TradePrice
                    this.TradePrice = Snapshot.Bid(1);
                end
            elseif this.LiveAmount < 0
                if this.TradePrice - Snapshot.Ask(1) <= this.StopLoss ...
                || this.TradePrice - Snapshot.Ask(1) >= this.TakeProfit ...
                || this.HoldPeriod == this.MaxHoldPeriod
                    Amount = -this.LiveAmount;
                elseif Snapshot.Ask(1) < this.TradePrice
                    this.TradePrice = Snapshot.Ask(1);
                end
            elseif ~isnan(this.Vals(1)) && this.TCount == 0
                MA = indicators(this.Vals, 'ema', this.MA_Lookback);
                Signal = CCRT(MA, this.CCRT_Lookback, this.CCRT_MinLag, this.CCRT_MaxLag);
                Signal = Signal(end - this.Signal_Lookbook + 1 : end);
                if sum(Signal > 0) == this.Signal_Lookbook
                    Amount = 1 - this.LiveAmount;
                    this.TradePrice = Snapshot.Bid(1);
                    this.HoldPeriod = 0;
                elseif sum(Signal < 0) == -this.Signal_Lookbook
                    Amount = -1 - this.LiveAmount;
                    this.TradePrice = Snapshot.Ask(1);
                    this.HoldPeriod = 0;
                end
            end
            
            if Amount ~= 0
                this.PlaceOrder(Snapshot.InstrumentID, Amount, 0, true);
            end

        end
        
        function OnOrder( this, Order )
        end
        
        function OnTrade( this, Trade )
            this.LiveAmount = this.LiveAmount + Trade.Amount;
        end
        
    end
    
end

