% Prepare Parameters
Contract = 'cu1511';
StartDate = datetime(2015, 9, 1);
EndDate = datetime(2015, 9, 20);
Lookback = 400;
Threshold = 3;

% Load data 
Data = SHFE_MatReader2(Contract, StartDate, EndDate);
Mid = (Data.Bid + Data.Ask) / 2;

% Calculate signals
Signal = -ZScoreMR(Mid, Lookback, Threshold);

AnalyseSignal(Mid, Signal);

