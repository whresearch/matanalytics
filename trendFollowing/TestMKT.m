% Prepare Parameters
Contract = 'cu1510';
StartDate = datetime(2015, 8, 20);
EndDate = datetime(2015, 8, 30);
Lookback = 120;
Alpha = 0.01;

% Load data 
Data = LoadMat_SHFE_Range(Contract, StartDate, EndDate);

% Calculate signals
Signal = MKT(Data.Mid, Lookback, Alpha);

% Analyse signals
AnalyseSignal(Data.Mid, Signal);

