% Prepare Parameters
Contract = 'cu1507';
StartDate = datetime(2015, 5, 1);
EndDate = datetime(2015, 5, 30);
Lookback = 60;
Alpha = 0.01;

% Load data 
Data = SHFE_MatReader2(Contract, StartDate, EndDate);
Mid = (Data.Bid + Data.Ask) / 2;

% Calculate signals
Signal = MKT(Mid, Lookback, Alpha);
AnalyseSignal(Mid, -Signal);

