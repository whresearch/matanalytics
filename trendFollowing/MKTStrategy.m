classdef MKTStrategy < Strategy
    %MKTSTRATEGY Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        Price_Interval = 1;
        MA_Lookback = 1;
        MKT_Lookback = 80;
        MKT_Alpha = 0.05;
        Signal_Lookbook = 1;
        MaxHoldPeriod = 10000000;
        StopLoss = -20;
        TakeProfit = 40;
        
        Vals;
        TradePrice;
        HoldPeriod;
        TCount;
        LiveAmount;
    end
    
    methods
        
        function this = MKTStrategy()
            this.Vals = NaN(this.Price_Interval * (this.MKT_Lookback + this.MA_Lookback * 10), 1);
            this.TradePrice = 0;
            this.HoldPeriod = 0;
            this.TCount = 0;
            this.LiveAmount = 0;
        end
        
        function OnMarketData( this, Snapshot )
            
            Amount = 0;
            
            this.TCount = this.TCount + 1;
            this.HoldPeriod = this.HoldPeriod + 1;
            
            if this.TCount == this.Price_Interval
                this.Vals(1:end-1) = this.Vals(2:end);
                this.Vals(end) = Snapshot.Mid;
                this.TCount = 0;
            end
            
            if this.LiveAmount > 0
                if Snapshot.Mid - this.TradePrice <= this.StopLoss ...
                || Snapshot.Mid - this.TradePrice >= this.TakeProfit ...
                || this.HoldPeriod == this.MaxHoldPeriod
                    Amount = -this.LiveAmount;
%                 elseif Snapshot.Mid > this.TradePrice
%                     this.TradePrice = Snapshot.Mid;
                end
            elseif this.LiveAmount < 0
                if this.TradePrice - Snapshot.Mid <= this.StopLoss ...
                || this.TradePrice - Snapshot.Mid >= this.TakeProfit ...
                || this.HoldPeriod == this.MaxHoldPeriod
                    Amount = -this.LiveAmount;
%                 elseif Snapshot.Ask(1) < this.TradePrice
%                     this.TradePrice = Snapshot.Mid;
                end
            end
            if ~isnan(this.Vals(1)) && this.TCount == 0
                % MA = indicators(this.Vals, 'ema', this.MA_Lookback);
                Signal = MKT(this.Vals, this.MKT_Lookback, this.MKT_Alpha);
                Signal = Signal(end - this.Signal_Lookbook + 1 : end);
                BidRatio = Snapshot.BidSize(1) / (Snapshot.BidSize(1) + Snapshot.AskSize(1));
                OpenRatio = Snapshot.OpenVolume / Snapshot.Volume;
                if BidRatio > 0.55 && OpenRatio > 0.55 && sum(Signal < 0) == this.Signal_Lookbook
                    Amount = 1 - this.LiveAmount;
                    this.TradePrice = Snapshot.Mid;
                    this.HoldPeriod = 0;
                elseif BidRatio < 0.45 && OpenRatio > 0.55 && sum(Signal > 0) == this.Signal_Lookbook
                    Amount = -1 - this.LiveAmount;
                    this.TradePrice = Snapshot.Mid;
                    this.HoldPeriod = 0;
                end
            end

            if Amount ~= 0
                this.PlaceOrder(Snapshot.InstrumentID, Amount, 0, true);
            end

        end
        
        function OnOrder( this, Order )
        end
        
        function OnTrade( this, Trade )
            this.LiveAmount = this.LiveAmount + Trade.Amount;
        end
        
    end
    
end

