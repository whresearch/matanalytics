function [ Trend, H ] = HET( Prices, Lookback, T, Threashold )

warning ('off','all');
        
n = length(Prices);
Trend = zeros(n, 1);
H = NaN(n, 1);

TL = log(T : Lookback);
    
for i = Lookback : n
    X = Prices(i - Lookback + 1 : i);
    m = mean(X);
    Y = X - m;
    
    RSM = NaN(Lookback - T + 1, 1);
    for t = T : Lookback
        RS = NaN(floor(Lookback / t), 1);
        for j = 1 : floor(Lookback / t)
            Z = cumsum(Y((j - 1) * t + 1 : j * t));
            R = max(Z) - min(Z);
            S = std(X((j - 1) * t + 1 : j * t));
            RS(j) = R / S;
        end
        RSM(t - T + 1) = mean(RS);
    end
    RSML = log(RSM);
    try
        mdl = fitlm(TL, RSML);
        H(i) = mdl.Coefficients.Estimate(2);
        if H(i) > Threashold
            Trend(i) = 1;
        end
    catch
        H(i) = nan;
        Trend(i) = nan;
    end
end

warning ('on','all');

end




        



