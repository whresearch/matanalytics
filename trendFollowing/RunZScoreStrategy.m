Contract = 'cu1511';
StartDate = datetime(2015, 9, 1);
EndDate = datetime(2015, 9, 20);
D = SHFE_MatReader2(Contract, StartDate, EndDate);
S = ZScoreStrategy();
B = RunBacktest(D, S);
