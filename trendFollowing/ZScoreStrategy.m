classdef ZScoreStrategy < Strategy
    %MKTSTRATEGY Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        Price_Interval = 1;
        MA_Lookback = 5;
        ZS_Lookback = 400;
        ZS_Threshold = 3;
        MaxHoldPeriod = 60;
        StopLoss = -60;
        TakeProfit = 100;
        
        Vals;
        TradePrice;
        HoldPeriod;
        TCount;
        LiveAmount;
    end
    
    methods
        
        function this = ZScoreStrategy()
            this.Vals = NaN(this.Price_Interval * (this.ZS_Lookback + this.MA_Lookback * 10), 1);
            this.TradePrice = 0;
            this.HoldPeriod = 0;
            this.TCount = 0;
            this.LiveAmount = 0;
        end
        
        function OnMarketData( this, Snapshot )
            
            Amount = 0;
            
            this.TCount = this.TCount + 1;
            this.HoldPeriod = this.HoldPeriod + 1;
            
            if this.TCount == this.Price_Interval
                this.Vals(1:end-1) = this.Vals(2:end);
                this.Vals(end) = Snapshot.Mid;
                this.TCount = 0;
            end
            
            if this.LiveAmount > 0
                if Snapshot.Bid - this.TradePrice <= this.StopLoss ...
                || Snapshot.Bid - this.TradePrice >= this.TakeProfit ...
                || this.HoldPeriod == this.MaxHoldPeriod
                    Amount = -this.LiveAmount;
                elseif Snapshot.Bid > this.TradePrice
                    this.TradePrice = Snapshot.Bid;
                end
            elseif this.LiveAmount < 0
                if this.TradePrice - Snapshot.Ask <= this.StopLoss ...
                || this.TradePrice - Snapshot.Ask >= this.TakeProfit ...
                || this.HoldPeriod == this.MaxHoldPeriod
                    Amount = -this.LiveAmount;
                elseif Snapshot.Ask < this.TradePrice
                    this.TradePrice = Snapshot.Ask;
                end
            elseif ~isnan(this.Vals(1)) && this.TCount == 0
                Signal = ZScoreMR(this.Vals, this.ZS_Lookback, this.ZS_Threshold);
                Signal = Signal(end);
                if Signal > 0
                    Amount = 1 - this.LiveAmount;
                    this.TradePrice = Snapshot.Bid;
                    this.HoldPeriod = 0;
                elseif Signal < 0
                    Amount = -1 - this.LiveAmount;
                    this.TradePrice = Snapshot.Ask;
                    this.HoldPeriod = 0;
                end
            end

            if Amount ~= 0
                this.PlaceOrder(Snapshot.InstrumentID, Amount, 0, true);
            end

        end
        
        function OnOrder( this, Order )
        end
        
        function OnTrade( this, Trade )
            this.LiveAmount = this.LiveAmount + Trade.Amount;
        end
        
    end
    
end

