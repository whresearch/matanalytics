function [ Signal, UpperBand, LowerBand, Vol, ZScore ] = ZScoreMR( Price, Lookback, Threashold )

n = length(Price);

MA = indicators(Price, 'sma', Lookback);
X = Price - MA;

Signal = NaN(n, 1);
UpperBand = NaN(n, 1);
LowerBand = NaN(n, 1);
Vol = NaN(n, 1);
ZScore = NaN(n, 1);

for i = Lookback : n
    Xtmp = X(i - Lookback + 1 : i);
    Var = sum(Xtmp .* Xtmp) / (Lookback - 1);
    Vol(i) = sqrt(Var);
    UpperBand(i) = MA(i) + Vol(i) * Threashold;
    LowerBand(i) = MA(i) - Vol(i) * Threashold;
    ZScore(i) = (Price(i) - MA(i)) / Vol(i);
    if min(ZScore(i - 5 : i - 1)) > Threashold && ZScore(i) <= Threashold
        Signal(i) = -1;
    elseif max(ZScore(i - 5 : i - 1)) < -Threashold && ZScore(i) >= -Threashold
        Signal(i) = 1;
    end
end

end




        



