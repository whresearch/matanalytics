function [ Trend ] = MKT( Xt, n, alpha )

len = length(Xt);
Trend = zeros(len, 1);
sigma = sqrt(1/18 * n * (n-1) * (2*n + 5));
Z_alpha=norminv(1 - alpha / 2, 0, 1);
for k = n : len
    count1 = 0;
    count2 = 0;
    count3 = 0;
    Xtmp = Xt(k - n + 1 : k);
    for i = 1 : n
        for j = i + 1 : n
            if Xtmp(j) > Xtmp(i)
                count1 = count1 + 1;
            elseif Xtmp(j) < Xtmp(i)
                count2 = count2 + 1;
            else
                count3 = count3 + 1;
            end
        end
    end
    SU = count1 - (count2 + count3);
    SD = count2 - (count1 + count3);
    if (SU - 1) / sigma > Z_alpha 
        Trend(k) = 1;
    elseif (SD - 1) / sigma > Z_alpha 
        Trend(k) = -1;
    end
end

end




        



