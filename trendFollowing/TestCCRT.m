% Prepare Parameters
Contract = 'cu1510';
StartDate = datetime(2015, 8, 20);
EndDate = datetime(2015, 8, 30);
Lookback = 40;
MinLag = 5;
MaxLag = 10;

% Load data 
Data = LoadMat_SHFE_Range(Contract, StartDate, EndDate);

% Calculate signals
Signal = CCRT(Data.Mid, Lookback, MinLag, MaxLag);

% Analyse signals
AnalyseSignal(Data.Mid, Signal);