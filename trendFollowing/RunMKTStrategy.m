Contract = 'cu1510';
StartDate = datetime(2015, 8, 20);
EndDate = datetime(2015, 8, 20);
Data = LoadMat_SHFE_Range(Contract, StartDate, EndDate);
Strategy = MKTStrategy();
Result = Backtest(Data, Strategy);
Result.ShowResult();
