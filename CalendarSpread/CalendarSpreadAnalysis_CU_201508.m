StartDate = datetime(2015, 8, 10);
EndDate = datetime(2015, 8, 30);
Contract = 'CU';
FrontMonth = datetime(2015, 9, 1);
EndMonth = datetime(2016, 7, 1);
Interval = 500;

NbMonth = months(FrontMonth, EndMonth);
Data = cell(NbMonth, 1);
for i = 1 : NbMonth
    Month = FrontMonth + calmonths(i);
    Symbol = [ Contract, datestr(Month, 'yymm') ];
    Data{i} = LoadMat_SHFE_Range(Symbol, StartDate, EndDate);
end

Length = Data{1}.Length;
MidSpread = NaN(NbMonth - 1, NbMonth - 1, Length);
for i = 1 : NbMonth - 1
    for j = 1 : NbMonth - i
        MidSpread(i, j, :) = Data{i}.Mid - Data{i + j}.Mid;
    end
end

MidVol = NaN(NbMonth - 1, NbMonth - 1, int32(Length / Interval));
MidVolAvg = NaN(NbMonth - 1, NbMonth - 1);
for i = 1 : NbMonth - 1
    for j = 1 : NbMonth - i
        for z = 1 : int32(Length / Interval)
            MidVol(i, j, z) = std(MidSpread(i, j, (z - 1) * Interval + 1 : z * Interval));
        end
        MidVolAvg(i, j) = nanmean(MidVol(i, j, :));
    end
end