StartDate = datetime(2015, 8, 20);
EndDate = datetime(2015, 8, 20);
Contract1 = 'CU1509';
Contract2 = 'CU1510';
BeginIndex = 1000;
EndIndex = 6001000;
Method = 'Quantile';

Data1 = LoadMat_SHFE_Range(Contract1, StartDate, EndDate);
Data2 = LoadMat_SHFE_Range(Contract2, StartDate, EndDate);
EndIndex = min(EndIndex, Data1.Length);
Length = EndIndex - BeginIndex;
X = transpose(1 : Length);

Data1_Spread = Data1.Ask(BeginIndex + 1 : EndIndex, 1) - Data1.Bid(BeginIndex + 1 : EndIndex, 1);
Data2_Spread = Data2.Ask(BeginIndex + 1 : EndIndex, 1) - Data2.Bid(BeginIndex + 1 : EndIndex, 1);
% figure('name', 'Bid/Ask Spreads');
% hold on;
% plot(X, Data1_Spread);
% plot(X, Data2_Spread);

% calculate spread
Spd_Mid = Data2.Mid(BeginIndex + 1 : EndIndex, 1) - Data1.Mid(BeginIndex + 1 : EndIndex, 1);
Spd_Ask = Data2.Ask(BeginIndex + 1 : EndIndex, 1) - Data1.Bid(BeginIndex + 1 : EndIndex, 1);
Spd_Bid = Data2.Bid(BeginIndex + 1 : EndIndex, 1) - Data1.Ask(BeginIndex + 1 : EndIndex, 1);

Entry_Upper = NaN(Length, 1);
Entry_Lower = NaN(Length, 1);
Exit_Upper = NaN(Length, 1);
Exit_Lower = NaN(Length, 1);
SL_Upper = NaN(Length, 1);
SL_Lower = NaN(Length, 1);

switch Method
    case 'MeanVol'
        % calculate mean & vol
        Interval = 500;
        EntryMultiplier = 2.5;
        ExitMultiplier = 2;
        SLMultipler = nan;
        Spd = Spd_Mid;
        Spd_Mean = indicators(Spd, 'sma', Interval);
        Spd_Vol = Spd - Spd_Mean;
        Spd_Vol = sqrt(Spd_Vol .* Spd_Vol);
        Spd_Vol = indicators(Spd_Vol, 'sma', Interval);
        Entry_Upper = Spd_Mean + EntryMultiplier * Spd_Vol;
        Entry_Lower = Spd_Mean - EntryMultiplier * Spd_Vol;
        Exit_Upper = Spd_Mean + ExitMultiplier * Spd_Vol;
        Exit_Lower = Spd_Mean - ExitMultiplier * Spd_Vol;
        SL_Upper = Spd_Mean + SLMultipler * Spd_Vol;
        SL_Lower = Spd_Mean - SLMultipler * Spd_Vol;
    case 'Quantile'
        % calculate quantile
        Interval = 500;
        EntryQuantile = 0.80;
        EntryShift = 10;
        ExitQuantile = 0.80;
        ExitShift = 10;
        SLQuantile = nan;
        SLShift = nan;
        for i = Interval : Length
            Spd = Spd_Mid(i - Interval + 1 : i);
            Entry_Upper(i) = quantile(Spd, EntryQuantile) + EntryShift;
            Entry_Lower(i) = quantile(Spd, 1 - EntryQuantile) - EntryShift;
            Exit_Upper(i) = quantile(Spd, ExitQuantile) + ExitShift;
            Exit_Lower(i) = quantile(Spd, 1 - ExitQuantile) - ExitShift;
            SL_Upper(i) = quantile(Spd, SLQuantile) + SLShift;
            SL_Lower(i) = quantile(Spd, 1 - SLQuantile) - SLShift;
        end
    otherwise
        error('unknown method');
end

% plot
figure('name', 'Calendar Spread');
hold on;
plot(X, Spd_Mid);
plot(X, Entry_Upper);
plot(X, Entry_Lower);
plot(X, Exit_Upper);
plot(X, Exit_Lower);
plot(X, SL_Upper);
plot(X, SL_Lower);

PnL = zeros(Length, 1);
Pos = 0;
NbTrades = 0;
for i = 1 : Length
    
    if i > 1
        PnL(i) = PnL(i - 1) + Pos * (Spd_Mid(i) - Spd_Mid(i - 1));
    end
    
    if Pos == 0
        if Spd_Mid(i) >= Entry_Upper(i) && (isnan(SL_Upper(i)) || Spd_Mid(i) < SL_Upper(i))
            Pos = -1;
            NbTrades = NbTrades + 1;
        elseif Spd_Mid(i) <= Entry_Lower(i) && (isnan(SL_Lower(i)) || Spd_Mid(i) > SL_Lower(i))
            Pos = 1;
            NbTrades = NbTrades + 1;
        end
    elseif Pos > 0
        if Spd_Mid(i) >= Exit_Upper(i) || Spd_Mid(i) <= SL_Lower(i)
            Pos = 0;
        end
    elseif Pos < 0
        if Spd_Mid(i) <= Exit_Lower(i) || Spd_Mid(i) >= SL_Upper(i)
            Pos = 0;
        end
    end
end
fprintf('Total PnL %i \n', PnL(end));
fprintf('Number of Trades %i \n', NbTrades);
fprintf('Avg PnL Per Trade %f \n', PnL(end) / NbTrades);
fprintf('Avg Spread Contract 1 %f \n', mean(Data1_Spread));
fprintf('Avg Spread Contract 2 %f \n', mean(Data2_Spread));

% plot
figure('name', 'Profit & Loss');
plot(PnL);