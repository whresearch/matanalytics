StartDate = datetime(2015, 8, 20);
EndDate = datetime(2015, 8, 20);
Contract1 = 'CU1509';
Contract2 = 'CU1510';
BeginIndex = 1;
EndIndex = 60000;

Data1 = LoadMat_SHFE_Range(Contract1, StartDate, EndDate);
Data2 = LoadMat_SHFE_Range(Contract2, StartDate, EndDate);
EndIndex = min(EndIndex, Data1.Length);
Length = EndIndex - BeginIndex;

% calculate spread
Spd = Data2.Mid(BeginIndex + 1 : EndIndex, 1) - Data1.Mid(BeginIndex + 1 : EndIndex, 1);

X = [ 0.05; 10 ];
Windows = transpose(100 : 100 : 5000);
PnLs = zeros(length(Windows), 1);
NbTrades = zeros(length(Windows), 1);

parfor i = 1 : length(Windows)
    
    [ PnL, NbTrade ] = StrategyCostFunction(@CalSpdQuantileSignal, X, Windows(i), Spd, 5, 1);
    PnLs(i) = -PnL;
    NbTrades(i) = NbTrade;

end

figure('Name', 'P&L');
plot(PnLs);