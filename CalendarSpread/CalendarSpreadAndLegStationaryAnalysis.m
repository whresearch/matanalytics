StartDate = datetime(2015, 8, 20);
EndDate = datetime(2015, 8,20);
Contract1 = 'CU1509';
Contract2 = 'CU1510';
BeginIndex = 1000;

EndIndex = 6001000;
Interval = 400;

Data1 = LoadMat_SHFE_Range(Contract1, StartDate, EndDate);
Data2 = LoadMat_SHFE_Range(Contract2, StartDate, EndDate);
EndIndex = min(EndIndex, Data1.Length);
Length = EndIndex - BeginIndex;
X = transpose(1 : Length);

% calculate spread
Mid1 = Data1.Mid(BeginIndex + 1 : EndIndex, 1);
Bid1 = Data1.Bid(BeginIndex + 1 : EndIndex, 1);
Ask1 = Data1.Ask(BeginIndex + 1 : EndIndex, 1);
VWAP1 = Data1.VWAP(BeginIndex + 1 : EndIndex, 1);
Mid2 = Data2.Mid(BeginIndex + 1 : EndIndex, 1);
Bid2 = Data2.Bid(BeginIndex + 1 : EndIndex, 1);
Ask2 = Data2.Ask(BeginIndex + 1 : EndIndex, 1);
VWAP2 = Data2.VWAP(BeginIndex + 1 : EndIndex, 1);
Spd_Mid = Mid2 - Mid1;
Spd_Ask = Ask2 - Bid1;
Spd_Bid = Bid2 - Ask1;
Spd_VWAP = NaN(Length, 1);

VWAP1Tmp = nan;
VWAP2Tmp = nan;
for i = 1 : Length
    if ~isnan(Data1.VWAP(BeginIndex + i))
        VWAP1Tmp = Data1.VWAP(BeginIndex + i);
        Spd_VWAP(i) = VWAP2Tmp - VWAP1Tmp;
    end
    if ~isnan(Data2.VWAP(BeginIndex + i))
        VWAP2Tmp = Data2.VWAP(BeginIndex + i);
        Spd_VWAP(i) = VWAP2Tmp - VWAP1Tmp;
    end
end

Signal1=zeros(Length, 1);
Signal2=zeros(Length, 1);
A = cumsum(ones(Interval, 1));
SL1 = NaN(Length, 1);
SL2 = NaN(Length, 1);
SMAv1 = NaN(Length, 1);
SMAv2 = NaN(Length, 1);
SMAVol1 = NaN(Length, 1);
SMAVol2 = NaN(Length, 1);

Skew1 = NaN(Length, 1);
Skew2 = NaN(Length, 1);
Kurt1 = NaN(Length, 1);
Kurt2 = NaN(Length, 1);

% Mid1Diff = [ 0; Mid1(2 : end) - Mid1(1 : end - 1) ];
% Mid2Diff = [ 0; Mid2(2 : end) - Mid2(1 : end - 1) ];
% Mid1Diff = Mid1Diff .* Mid1Diff;
% Mid2Diff = Mid2Diff .* Mid2Diff;

% calculate stationary of both legs
for i = Interval : Length
    % calculate mean and vol
    PriceTmp1 = Mid1(i - Interval + 1 : i);
    PriceTmp2 = Mid2(i - Interval + 1 : i);
    % SL1(i) = (PriceTmp1(end) - PriceTmp1(1)) / Interval;
    % SL2(i) = (PriceTmp2(end) - PriceTmp2(1)) / Interval;
%     SL1(i) = (PriceTmp1 - PriceTmp1(1)) \ A;
%     SL2(i) = (PriceTmp2 - PriceTmp2(1)) \ A;
%     Signal1(i) = adftest(PriceTmp1 - mean(PriceTmp1));
%     Signal2(i) = adftest(PriceTmp2 - mean(PriceTmp2));
    
%     SMAv1(i) = mean(PriceTmp1);
%     SMAv2(i) = mean(PriceTmp2);
%     SMAVol1(i) = tsmovavg(PriceTmp1, 'e', 100, 1);
%     SMAVol2(i) = tsmovavg(PriceTmp2, 'e', 100, 1);

    Skew1(i) = skewness(PriceTmp1);
    Skew2(i) = skewness(PriceTmp2);
    Kurt1(i) = kurtosis(PriceTmp1);
    Kurt2(i) = kurtosis(PriceTmp2);
    
    %Vol1 = sum(abs(PriceTmp1(2 : end) - PriceTmp1(1 : end - 1))) / (length(PriceTmp1) - 1);
    %Vol2 = sum(abs(PriceTmp2(2 : end) - PriceTmp2(1 : end - 1))) / (length(PriceTmp2) - 1);
    
end
% SMAVol1 = sqrt(tsmovavg(Mid1Diff, 'e', Interval, 1));
% SMAVol2 = sqrt(tsmovavg(Mid2Diff, 'e', Interval, 1));

% Signal1(abs(SL1) < 4) = 1;
% Signal2(abs(SL2) < 4) = 1;
% Signal1(SMAVol1 < 2) = 1;
% Signal2(SMAVol2 < 2) = 1;

% plot prices and spread
figure('Name', 'Spread and Prices');
ax1 = subplot(3, 1, 1);
hold on;
plot(X, Spd_Mid);
scatter(X, Spd_VWAP, 10, 'm', 'filled');
ax2 = subplot(3, 1, 2);
hold on;
plotyy(X, Mid2, X, Skew2);
%plot(X, Mid2);
%scatter(find(Signal2 == 1), Mid2(Signal2 == 1), 10, 'g', 'filled');
% scatter(X, VWAP2, 10, 'm', 'filled');
ax3 = subplot(3, 1, 3);
hold on;
plotyy(X, Mid1, X, Skew1);
% plot(X, Mid1);
% scatter(find(Signal1 == 1), Mid1(Signal1 == 1), 10, 'g', 'filled');
% scatter(X, VWAP1, 10, 'm', 'filled');

linkaxes([ ax1 ax2 ax3 ], 'x');