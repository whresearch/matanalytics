Contract1 = 'cu1509';
Contract2 = 'cu1510';
StartDate = datetime(2015, 8, 20);
EndDate = datetime(2015, 8, 20);
Data1 = LoadMat_SHFE_Range(Contract1, StartDate, EndDate);
Data2 = LoadMat_SHFE_Range(Contract2, StartDate, EndDate);
Strategy = CSAStrategy2();
Result = Backtest([ Data1; Data2 ], Strategy);

Result.ShowSpreadResult(1, 2);
