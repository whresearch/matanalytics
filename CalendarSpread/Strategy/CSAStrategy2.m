classdef CSAStrategy2 < Strategy
    %MKTSTRATEGY Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        TickSize = 10;
        Lookback = 600;
        EntryQuantile = 0.05;
        EntryShift = 0.5;
        ExitLimitProfit = 4;
        ExitLimitProfitMin = 2;
        ExitMarketProfit = 6;
        ExitMarketProfitMin = 6;
        ExitMaxHoldPeriod = 600;
        ExitStopLoss = -3;
        
        LastTime;
        Spreads;
        Snapshots;
        
        % 0 - clean | exit both sides filled
        % 1 - entry orders submitted
        % 2 - entry one side filled
        % 3 - entry both sides filled
        % 4 - exit orders submitted
        % 5 - entry one side filled
        TradeStatus;
        Orders;
        Trades;
        Positions;
        LongSpread;
        EntrySpread;
        EntryLeg1;
        EntryLeg2;
        PendingPeriod;
        HoldingPeriod;
    end
    
    methods
        
        function this = CSAStrategy2()
            this.LastTime = datetime;
            this.Spreads = NaN(this.Lookback, 1);
            this.Snapshots = cell(2, 1);
            
            this.TradeStatus = 0;
            this.Orders = cell(2, 1);
            this.Trades = cell(2, 1);
            this.Positions = zeros(2, 1);
            this.PendingPeriod = 0;
            this.HoldingPeriod = 0;
        end
        
        function OnMarketData( this, Snapshot )
            
            this.Snapshots{Snapshot.InstrumentID} = Snapshot;
            
            % calculate spread
            if Snapshot.Time == this.LastTime
                this.Spreads(1 : end - 1) = this.Spreads(2 : end);
                this.Spreads(end) = this.Snapshots{2}.Mid - this.Snapshots{1}.Mid;
                this.HoldingPeriod = this.HoldingPeriod + 1;
                this.PendingPeriod = this.PendingPeriod + 1;
            else
                this.LastTime = Snapshot.Time;
                return;
            end
            
            if ~isnan(this.Spreads(1))
                
                CurrentSpread = this.Spreads(end);

                switch this.TradeStatus
                    case 0 % clean | both sides exited

                        Levels = sort(unique(this.Spreads));
                        MidSpread = quantile(this.Spreads, 0.5);
                        EntrySpreads = NaN(2, 1);
                        
                        % calculate entry levels
                        for j = 1 : find(Levels <= MidSpread, 1, 'last')
                            if sum(this.Spreads == Levels(j)) / length(this.Spreads) >= this.EntryQuantile
                                EntrySpreads(1) = Levels(j) - this.EntryShift * this.TickSize;
                                break;
                            end
                        end
                        if isnan(EntrySpreads(1))
                            EntrySpreads(1) = MidSpread - this.EntryShift * this.TickSize;
                        end
                        for j = length(Levels) : -1 : find(Levels >= MidSpread, 1, 'first')
                            if sum(this.Spreads == Levels(j)) / length(this.Spreads) >= this.EntryQuantile
                                EntrySpreads(2) = Levels(j) + this.EntryShift * this.TickSize;
                                break;
                            end
                        end
                        if isnan(EntrySpreads(2))
                            EntrySpreads(2) = MidSpread + this.EntryShift * this.TickSize;
                        end
                        
                        % place order
                        if CurrentSpread <= EntrySpreads(1)
                            this.LongSpread = true;
                            this.EntrySpread = EntrySpreads(1);
                            this.PendingPeriod = 0;
                            this.Orders = cell(2, 1);
                            this.Trades = cell(2, 1);
                            this.EntryLeg1 = this.Snapshots{1}.Mid;
                            this.EntryLeg2 = this.Snapshots{2}.Mid;
                            this.PlaceOrder(this.Snapshots{1}.InstrumentID, -1, this.Snapshots{1}.Ask, false);
                            this.PlaceOrder(this.Snapshots{2}.InstrumentID, 1, this.Snapshots{2}.Bid, false);
                        elseif CurrentSpread >= EntrySpreads(2)
                            this.LongSpread = false;
                            this.EntrySpread = EntrySpreads(2);
                            this.PendingPeriod = 0;
                            this.Orders = cell(2, 1);
                            this.Trades = cell(2, 1);
                            this.EntryLeg1 = this.Snapshots{1}.Mid;
                            this.EntryLeg2 = this.Snapshots{2}.Mid;
                            this.PlaceOrder(this.Snapshots{1}.InstrumentID, 1, this.Snapshots{1}.Bid, false);
                            this.PlaceOrder(this.Snapshots{2}.InstrumentID, -1, this.Snapshots{2}.Ask, false);
                        end
                        
                    case 1 % entry orders submitted

                        CancelEntry = false;
                        
                        if this.PendingPeriod >= 500
                            CancelEntry = true;
                        else
                            if this.LongSpread
                                if CurrentSpread > this.EntrySpread
                                    CancelEntry = true;
                                end
                            else
                                if CurrentSpread < this.EntrySpread
                                    CancelEntry = true;
                                end
                            end
                        end

                        if CancelEntry
                            this.CancelOrder(this.Orders{1});
                            this.CancelOrder(this.Orders{2});
                        end

                    case 2 % one side filled

                        if this.Orders{1}.IsActive
                            if (this.LongSpread && this.Snapshots{1}.Ask < this.Orders{1}.Price) ...
                            || (~this.LongSpread && this.Snapshots{1}.Bid > this.Orders{1}.Price)
                                this.CancelOrder(this.Orders{1});
                                TempSpread = this.EntryLeg2 - this.Snapshots{1}.Mid;
                                if (this.LongSpread && TempSpread > this.EntrySpread) ...
                                || (~this.LongSpread && TempSpread < this.EntrySpread) ...
                                || this.Snapshots{1}.Ask - this.Snapshots{1}.Bid > this.TickSize
                                    this.PlaceOrder(this.Orders{2}.InstrumentID, -sum(this.Orders{2}.FilledAmount), 0, false);
                                else
                                    this.PlaceOrder(this.Orders{1}.InstrumentID, this.Orders{1}.RemainingAmount, 0, false);
                                end
                            end
                        elseif this.Orders{2}.IsActive
                            if (this.LongSpread && this.Snapshots{2}.Bid > this.Orders{2}.Price) ...
                            || (~this.LongSpread && this.Snapshots{2}.Ask < this.Orders{2}.Price)
                                this.CancelOrder(this.Orders{2});
                                TempSpread = this.Snapshots{2}.Mid - this.EntryLeg1;
                                if (this.LongSpread && TempSpread > this.EntrySpread) ...
                                || (~this.LongSpread && TempSpread < this.EntrySpread) ...
                                || this.Snapshots{2}.Ask - this.Snapshots{2}.Bid > this.TickSize
                                    this.PlaceOrder(this.Orders{1}.InstrumentID, -sum(this.Orders{1}.FilledAmount), 0, false);
                                else
                                    this.PlaceOrder(this.Orders{2}.InstrumentID, this.Orders{2}.RemainingAmount, 0, false);
                                end
                            end
                        end

                    case 3 % both sides filled

                        ExitMarketRatio = this.ExitMarketProfit + (this.ExitMarketProfitMin - ...
                            this.ExitMarketProfit) / this.ExitMaxHoldPeriod * this.HoldingPeriod;
                        ExitLimitRatio = this.ExitLimitProfit + (this.ExitLimitProfitMin - ...
                            this.ExitLimitProfit) / this.ExitMaxHoldPeriod * this.HoldingPeriod;
                        
                        % place exit orders
                        if this.LongSpread
                            TargetExitLimitSpread = this.EntrySpread + ExitLimitRatio * this.TickSize;
                            TargetExitMarketSpread = this.EntrySpread + ExitMarketRatio * this.TickSize;
                            if CurrentSpread >= TargetExitMarketSpread
                                this.PlaceOrder(this.Snapshots{1}.InstrumentID, 1, 0, false);
                                this.PlaceOrder(this.Snapshots{2}.InstrumentID, -1, 0, false);
                                this.PendingPeriod = 0;
                                this.Orders = cell(2, 1);
                                this.Trades = cell(2, 1);
                            elseif CurrentSpread >= TargetExitLimitSpread
                                this.PlaceOrder(this.Snapshots{1}.InstrumentID, 1, this.Snapshots{1}.Bid, false);
                                this.PlaceOrder(this.Snapshots{2}.InstrumentID, -1, this.Snapshots{2}.Ask, false);
                                this.PendingPeriod = 0;
                                this.Orders = cell(2, 1);
                                this.Trades = cell(2, 1);
                            end
                        else
                            TargetExitLimitSpread = this.EntrySpread - ExitLimitRatio * this.TickSize;
                            TargetExitMarketSpread = this.EntrySpread - ExitMarketRatio * this.TickSize;
                            if CurrentSpread <= TargetExitMarketSpread
                                this.PlaceOrder(this.Snapshots{1}.InstrumentID, -1, 0, false);
                                this.PlaceOrder(this.Snapshots{2}.InstrumentID, 1, 0, false);
                                this.PendingPeriod = 0;
                                this.Orders = cell(2, 1);
                                this.Trades = cell(2, 1);
                            elseif CurrentSpread <= TargetExitLimitSpread
                                this.PlaceOrder(this.Snapshots{1}.InstrumentID, -1, this.Snapshots{1}.Ask, false);
                                this.PlaceOrder(this.Snapshots{2}.InstrumentID, 1, this.Snapshots{2}.Bid, false);
                                this.PendingPeriod = 0;
                                this.Orders = cell(2, 1);
                                this.Trades = cell(2, 1);
                            end
                        end

                    case 4 % exit orders submitted
                        
                        ExitMarketRatio = this.ExitMarketProfit + (this.ExitMarketProfitMin - ...
                            this.ExitMarketProfit) / this.ExitMaxHoldPeriod * this.HoldingPeriod;
                        ExitLimitRatio = this.ExitLimitProfit + (this.ExitLimitProfitMin - ...
                            this.ExitLimitProfit) / this.ExitMaxHoldPeriod * this.HoldingPeriod;
                        
                        % place exit orders
                        if this.LongSpread
                            TargetExitLimitSpread = this.EntrySpread + ExitLimitRatio * this.TickSize;
                            TargetExitMarketSpread = this.EntrySpread + ExitMarketRatio * this.TickSize;
                            if CurrentSpread >= TargetExitMarketSpread
                                this.CancelOrder(this.Orders{1});
                                this.CancelOrder(this.Orders{2});
                                this.PlaceOrder(this.Snapshots{1}.InstrumentID, 1, 0, false);
                                this.PlaceOrder(this.Snapshots{2}.InstrumentID, -1, 0, false);
                            elseif CurrentSpread >= TargetExitLimitSpread
                                if this.Snapshots{1}.Bid ~= this.Orders{1}.Price
                                    this.CancelOrder(this.Orders{1});
                                    this.PlaceOrder(this.Snapshots{1}.InstrumentID, 1, this.Snapshots{1}.Bid, false);
                                end
                                if this.Snapshots{2}.Ask ~= this.Orders{2}.Price
                                    this.CancelOrder(this.Orders{2});
                                    this.PlaceOrder(this.Snapshots{2}.InstrumentID, -1, this.Snapshots{2}.Ask, false);
                                end
                            end
                        else
                            TargetExitLimitSpread = this.EntrySpread - ExitLimitRatio * this.TickSize;
                            TargetExitMarketSpread = this.EntrySpread - ExitMarketRatio * this.TickSize;
                            if CurrentSpread <= TargetExitMarketSpread
                                this.CancelOrder(this.Orders{1});
                                this.CancelOrder(this.Orders{2});
                                this.PlaceOrder(this.Snapshots{1}.InstrumentID, -1, 0, false);
                                this.PlaceOrder(this.Snapshots{2}.InstrumentID, 1, 0, false);
                            elseif CurrentSpread <= TargetExitLimitSpread
                                if this.Snapshots{1}.Ask ~= this.Orders{1}.Price
                                    this.CancelOrder(this.Orders{1});
                                    this.PlaceOrder(this.Snapshots{1}.InstrumentID, -1, this.Snapshots{1}.Ask, false);
                                end
                                if this.Snapshots{2}.Bid ~= this.Orders{2}.Price
                                    this.CancelOrder(this.Orders{2});
                                    this.PlaceOrder(this.Snapshots{2}.InstrumentID, 1, this.Snapshots{2}.Bid, false);
                                end
                            end
                        end
                        
                    case 5 % one side exited

                        if this.Orders{1}.IsActive
                            if (this.LongSpread && this.Snapshots{1}.Bid > this.Orders{1}.Price) ...
                            || (~this.LongSpread && this.Snapshots{1}.Ask < this.Orders{1}.Price)
                                this.CancelOrder(this.Orders{1});
                                this.PlaceOrder(this.Orders{1}.InstrumentID, this.Orders{1}.RemainingAmount, 0, true);
                            end
                        elseif this.Orders{2}.IsActive
                            if (this.LongSpread && this.Snapshots{2}.Ask < this.Orders{2}.Price) ...
                            || (~this.LongSpread && this.Snapshots{2}.Bid > this.Orders{2}.Price)
                                this.CancelOrder(this.Orders{2});
                                this.PlaceOrder(this.Orders{2}.InstrumentID, this.Orders{2}.RemainingAmount, 0, true);
                            end
                        end

                end

            end
            
        end
        
        function OnOrder( this, Order )
            
            if isempty(this.Orders{Order.InstrumentID}) || this.Orders{Order.InstrumentID} ~= Order
                if ~isempty(this.Orders{Order.InstrumentID}) ...
                && this.Orders{Order.InstrumentID}.IsActive
                    error('should not reach here');
                end
                this.Orders{Order.InstrumentID} = Order;
            end
            
            if ~isempty(this.Orders{1}) && ~isempty(this.Orders{2})
                switch this.TradeStatus
                    case 0 % clean | both sides exited
                        if this.Orders{1}.Status == OrderStatus.Pending && this.Orders{2}.Status == OrderStatus.Pending
                            this.TradeStatus = 1;
                        end
                    case 1 % entry orders submitted
                        if this.Orders{1}.Status == OrderStatus.Cancelled && this.Orders{2}.Status == OrderStatus.Cancelled
                            this.TradeStatus = 0;
                        end
                    case 3 % both sides filled
                        if this.Orders{1}.Status == OrderStatus.Pending && this.Orders{2}.Status == OrderStatus.Pending
                            this.TradeStatus = 4;
                        end
                    case 4 % exit orders submitted
                    case 5 % one side exited
                end
            end
            
        end
        
        function OnTrade( this, Trade )
            
            this.Positions(Trade.InstrumentID) = this.Positions(Trade.InstrumentID) + Trade.Amount;
            this.Trades{Trade.InstrumentID} = Trade;
            
            switch this.TradeStatus
                case 0 % clean | both sides exited
                    if sum(this.Positions == 0) == 1
                        this.TradeStatus = 2;
                    elseif sum(this.Positions == 0) == 0
                        this.TradeStatus = 3;
                    end
                case 1 % entry orders submitted
                    if sum(this.Positions == 0) == 1
                        this.TradeStatus = 2;
                    elseif sum(this.Positions == 0) == 0
                        this.TradeStatus = 3;
                    end
                case 2 % one side filled
                    if sum(this.Positions == 0) == 2
                        this.TradeStatus = 0;
                    elseif sum(this.Positions == 0) == 0
                        this.TradeStatus = 3;
                    end
                case 3 % both sides filled
                    if sum(this.Positions == 0) == 1
                        this.TradeStatus = 5;
                    elseif sum(this.Positions == 0) == 2
                        this.TradeStatus = 0;
                    end
                case 4 % exit orders submitted
                    if sum(this.Positions == 0) == 1
                        this.TradeStatus = 5;
                    elseif sum(this.Positions == 0) == 2
                        this.TradeStatus = 0;
                    end
                case 5 % one side exited
                    if sum(this.Positions == 0) == 2
                        this.TradeStatus = 0;
                    end
            end
            
            if this.TradeStatus == 3
                this.HoldingPeriod = 0;
                % this.EntrySpread = this.Trades{2}.Price - this.Trades{1}.Price;
            end
            
        end
        
    end
    
end

