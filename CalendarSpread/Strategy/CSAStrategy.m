classdef CSAStrategy < Strategy
    %MKTSTRATEGY Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        Lookback = 600;
        EntryQuantile = 0.05;
        EntryShift = 10;
        ExitQuantile = 0.05;
        ExitShift = 10;
        
        MaxHoldPeriod = nan;
        StopLoss = -50;
        TakeProfit = 100;
        
        Snapshots;
        Orders;
        LastTime;
        MidSpreads;
        TradePrices;
        EntrySpread;
        ExitSpread;
        Direction;
        HoldPeriod;
        LiveAmount;
        
        % 0 - clean | both sides exited
        % 1 - entry orders submitted
        % 2 - one side filled
        % 3 - both sides filled
        % 4 - exit orders submitted
        % 5 - one side exited
        TradeStatus;
    end
    
    methods
        
        function this = CSAStrategy()
            this.Snapshots = cell(2, 1);
            this.Orders = cell(2, 1);
            this.LastTime = datetime(0, 0, 0);
            this.MidSpreads = NaN(this.Lookback, 1);
            this.TradePrices = NaN(2, 1);
            this.EntrySpread = 0;
            this.ExitSpread = 0;
            this.Direction = 0;
            this.HoldPeriod = 0;
            this.LiveAmount = zeros(2, 1);
            
            this.TradeStatus = 0;
        end
        
        function OnMarketData( this, Snapshot )
            
            if Snapshot.Time > this.LastTime
                this.MidSpreads(1 : end - 1) = this.MidSpreads(2 : end);
                this.MidSpreads(end) = nan;
                this.Snapshots = cell(2, 1);
                this.LastTime = Snapshot.Time;
            end
            
            this.Snapshots{Snapshot.InstrumentID} = Snapshot;
            this.HoldPeriod = this.HoldPeriod + 1;
            
            if ~isempty(this.Snapshots{1}) && ~isempty(this.Snapshots{2})
                
                % calculate spread
                this.MidSpreads(end) = this.Snapshots{2}.Mid - this.Snapshots{1}.Mid;
                
                if ~isnan(this.MidSpreads(1))
                    
                    Levels = sort(unique(this.MidSpreads));
                    
                    switch this.TradeStatus
                        case 0 % clean | both sides exited
                            
                            Spd = this.MidSpreads(end);
                            Mid = quantile(this.MidSpreads, 0.5);
                            % calculate entry levels
                            if Spd < Mid
                                LowerEntry = nan;
                                for j = 1 : find(Levels <= Mid - this.EntryShift, 1, 'last')
                                    if sum(this.MidSpreads == Levels(j)) / length(this.MidSpreads) >= this.EntryQuantile
                                        LowerEntry = Levels(j) - this.EntryShift;
                                        break;
                                    end
                                end
                                if isnan(LowerEntry)
                                    LowerEntry = Mid - this.EntryShift;
                                end
                                if Spd <= LowerEntry
                                    this.PlaceOrder(this.Snapshots{2}.InstrumentID, 1, this.Snapshots{2}.Bid, false);
                                    this.PlaceOrder(this.Snapshots{1}.InstrumentID, -1, this.Snapshots{1}.Ask, false);
%                                     this.PlaceOrder(this.Snapshots{2}.InstrumentID, 1, 0, true);
%                                     this.PlaceOrder(this.Snapshots{1}.InstrumentID, -1, 0, true);
                                    this.Direction = 1;
                                    this.HoldPeriod = 0;
                                    this.TradePrices(1) = this.Snapshots{1}.Mid;
                                    this.TradePrices(2) = this.Snapshots{2}.Mid;
                                    this.EntrySpread = LowerEntry;
                                end
                            else
                                UpperEntry = nan;
                                for j = length(Levels) : -1 : find(Levels >= Mid + this.EntryShift, 1, 'first')
                                    if sum(this.MidSpreads == Levels(j)) / length(this.MidSpreads) >= this.EntryQuantile
                                        UpperEntry = Levels(j) + this.EntryShift;
                                        break;
                                    end
                                end
                                if isnan(UpperEntry)
                                    UpperEntry = Mid + this.EntryShift;
                                end
                                if Spd >= UpperEntry
                                    this.PlaceOrder(this.Snapshots{2}.InstrumentID, -1, this.Snapshots{2}.Ask, false);
                                    this.PlaceOrder(this.Snapshots{1}.InstrumentID, 1, this.Snapshots{1}.Bid, false);
%                                     this.PlaceOrder(this.Snapshots{1}.InstrumentID, -1, 0, true);
%                                     this.PlaceOrder(this.Snapshots{2}.InstrumentID, 1, 0, true);
                                    this.Direction = -1;
                                    this.HoldPeriod = 0;
                                    this.TradePrices(1) = this.Snapshots{1}.Mid;
                                    this.TradePrices(2) = this.Snapshots{2}.Mid;
                                    this.EntrySpread = UpperEntry;
                                end
                            end
                            
                        case 1 % entry orders submitted
                            
                            Cancel = false;
                            Spd = this.MidSpreads(end);
                            
                            if this.HoldPeriod == 500
                                Cancel = true;
                            else
                                if this.Direction > 0
                                    if Spd > this.EntrySpread
                                        Cancel = true;
                                    end
                                elseif this.Direction < 0
                                    if Spd < this.EntrySpread
                                        Cancel = true;
                                    end
                                end
                            end
                            
                            if Cancel
                                this.CancelOrder(this.Orders{1});
                                this.CancelOrder(this.Orders{2});
                            end
                            
                        case 2 % one side filled
                            
                            if this.Orders{1}.IsActive
                                this.CancelOrder(this.Orders{1});
                                Spd = this.TradePrices(2) - this.Snapshots{1}.Mid;
                                if (this.Direction > 0 && Spd > this.EntrySpread) ...
                                || (this.Direction < 0 && Spd < this.EntrySpread) ...
                                || this.Snapshots{1}.Ask - this.Snapshots{1}.Bid > 10
                                    this.PlaceOrder(this.Orders{2}.InstrumentID, -this.Orders{2}.Amount, 0, false);
                                else
                                    this.PlaceOrder(this.Orders{1}.InstrumentID, this.Orders{1}.Amount, 0, false);
                                end
                            elseif this.Orders{2}.IsActive
                                this.CancelOrder(this.Orders{2});
                                Spd = this.Snapshots{2}.Mid - this.TradePrices(1);
                                if (this.Direction > 0 && Spd > this.EntrySpread) ...
                                || (this.Direction < 0 && Spd < this.EntrySpread) ...
                                || this.Snapshots{2}.Ask - this.Snapshots{2}.Bid > 10
                                    this.PlaceOrder(this.Orders{1}.InstrumentID, -this.Orders{1}.Amount, 0, false);
                                else
                                    this.PlaceOrder(this.Orders{2}.InstrumentID, this.Orders{2}.Amount, 0, false);
                                end
                            end
                            
                        case 3 % both sides filled
                            
%                             Spd = this.MidSpreads(end);
%                             
%                             % calculate exit levels
%                             if this.Direction < 0
%                                 LowerExit = quantile(Levels, this.ExitQuantile) - this.ExitShift;
%                                 if Spd <= LowerExit
%                                     this.PlaceOrder(this.Snapshots{2}.InstrumentID, 1, 0, false);
%                                     this.PlaceOrder(this.Snapshots{1}.InstrumentID, -1, 0, false);
%                                     this.Direction = 0;
%                                     this.HoldPeriod = 0;
%                                 end
%                             elseif this.Direction > 0
%                                 UpperExit = quantile(Levels, 1 - this.ExitQuantile) + this.ExitShift;
%                                 if Spd >= UpperExit
%                                     this.PlaceOrder(this.Snapshots{2}.InstrumentID, -1, 0, false);
%                                     this.PlaceOrder(this.Snapshots{1}.InstrumentID, 1, 0, false);
%                                     this.Direction = 0;
%                                     this.HoldPeriod = 0;
%                                 end
%                             end

                            Spd = this.MidSpreads(end);
                            Mid = quantile(this.MidSpreads, 0.5);
                            if this.Snapshots{1}.Ask - this.Snapshots{1}.Bid == 10 ...
                            || this.Snapshots{2}.Ask - this.Snapshots{2}.Bid == 10
                                % calculate exit levels
                                if this.Direction < 0 && Spd <= Mid
                                    LowerExit = nan;
                                    for j = 1 : find(Levels <= Mid - this.ExitShift, 1, 'last')
                                        if sum(this.MidSpreads == Levels(j)) / length(this.MidSpreads) >= this.ExitQuantile
                                            LowerExit = Levels(j) - this.ExitShift;
                                            break;
                                        end
                                    end
                                    if isnan(LowerExit)
                                        LowerExit = Mid - this.ExitShift;
                                    end
                                    if Spd <= LowerExit
                                        this.PlaceOrder(this.Snapshots{2}.InstrumentID, 1, this.Snapshots{2}.Bid, false);
                                        this.PlaceOrder(this.Snapshots{1}.InstrumentID, -1, this.Snapshots{1}.Ask, false);
%                                         this.PlaceOrder(this.Snapshots{2}.InstrumentID, 1, 0, false);
%                                         this.PlaceOrder(this.Snapshots{1}.InstrumentID, -1, 0, false);
                                        this.HoldPeriod = 0;
                                        this.ExitSpread = LowerExit;
                                    end
                                elseif this.Direction > 0 && Spd >= Mid
                                    UpperExit = nan;
                                    for j = length(Levels) : -1 : find(Levels >= Mid + this.ExitShift, 1, 'first')
                                        if sum(this.MidSpreads == Levels(j)) / length(this.MidSpreads) >= this.ExitQuantile
                                            UpperExit = Levels(j) + this.ExitShift;
                                            break;
                                        end
                                    end
                                    if isnan(UpperExit)
                                        UpperExit = Mid + this.ExitShift;
                                    end
                                    if Spd >= UpperExit
                                        this.PlaceOrder(this.Snapshots{2}.InstrumentID, -1, this.Snapshots{2}.Ask, false);
                                        this.PlaceOrder(this.Snapshots{1}.InstrumentID, 1, this.Snapshots{1}.Bid, false);
%                                         this.PlaceOrder(this.Snapshots{2}.InstrumentID, -1, 0, false);
%                                         this.PlaceOrder(this.Snapshots{1}.InstrumentID, 1, 0, false);
                                        this.HoldPeriod = 0;
                                        this.ExitSpread = UpperExit;
                                    end
                                end
                            end
                            
                        case 4 % exit orders submitted
                            
                            MarketExecute = false;
                            Spd = this.MidSpreads(end);
                            
                            if this.HoldPeriod == 500
                                MarketExecute = true;
                            else
                                if this.Direction > 0
                                    if Spd >= this.ExitSpread + 10
                                        MarketExecute = true;
                                    end
                                elseif this.Direction < 0
                                    if Spd <= this.ExitSpread - 10
                                        MarketExecute = true;
                                    end
                                end
                            end
                            
                            if MarketExecute
                                this.CancelOrder(this.Orders{1});
                                this.CancelOrder(this.Orders{2});
                                this.PlaceOrder(this.Orders{1}.InstrumentID, this.Orders{1}.Amount, 0, false);
                                this.PlaceOrder(this.Orders{2}.InstrumentID, this.Orders{2}.Amount, 0, false);
                            end
                            
                        case 5 % one side exited
                            
                            if this.Orders{1}.IsActive
                                this.CancelOrder(this.Orders{1});
                                this.PlaceOrder(this.Orders{1}.InstrumentID, this.Orders{1}.Amount, 0, false);
                            elseif this.Orders{2}.IsActive
                                this.CancelOrder(this.Orders{2});
                                this.PlaceOrder(this.Orders{2}.InstrumentID, this.Orders{2}.Amount, 0, false);
                            end
                            
                    end
                    
                end
            end
            
        end
        
        function OnOrder( this, Order )
            this.Orders{Order.InstrumentID} = Order;
            switch this.TradeStatus
                case 0 % clean | both sides exited
                    if ~isempty(this.Orders{1}) && ~isempty(this.Orders{2})
                        if strcmp(this.Orders{1}.Status, 'Filled') && strcmp(this.Orders{2}.Status, 'Filled')
                            this.TradeStatus = 3;
                            this.Orders = cell(2, 1);
                        elseif strcmp(this.Orders{1}.Status, 'Filled') || strcmp(this.Orders{2}.Status, 'Filled')
                            this.TradeStatus = 2;
                        elseif strcmp(this.Orders{1}.Status, 'Live') && strcmp(this.Orders{2}.Status, 'Live')
                            this.TradeStatus = 1;
                        end
                    end
                case 1 % entry orders submitted
                    if strcmp(this.Orders{1}.Status, 'Cancelled') && strcmp(this.Orders{2}.Status, 'Cancelled')
                        this.TradeStatus = 0;
                        this.Orders = cell(2, 1);
                    elseif strcmp(this.Orders{1}.Status, 'Filled') && strcmp(this.Orders{2}.Status, 'Filled')
                        this.TradeStatus = 3;
                        this.Orders = cell(2, 1);
                    elseif strcmp(this.Orders{1}.Status, 'Filled') || strcmp(this.Orders{2}.Status, 'Filled')
                        this.TradeStatus = 2;
                    end
                case 2 % one side filled
                case 3 % both sides filled
                    if ~isempty(this.Orders{1}) && ~isempty(this.Orders{2})
                        if strcmp(this.Orders{1}.Status, 'Filled') && strcmp(this.Orders{2}.Status, 'Filled')
                            this.TradeStatus = 0;
                            this.Orders = cell(2, 1);
                        elseif strcmp(this.Orders{1}.Status, 'Filled') || strcmp(this.Orders{2}.Status, 'Filled')
                            this.TradeStatus = 5;
                        elseif strcmp(this.Orders{1}.Status, 'Live') && strcmp(this.Orders{2}.Status, 'Live')
                            this.TradeStatus = 4;
                        end
                    end
                case 4 % exit orders submitted
                    if strcmp(this.Orders{1}.Status, 'Filled') && strcmp(this.Orders{2}.Status, 'Filled')
                        this.TradeStatus = 0;
                        this.Orders = cell(2, 1);
                    elseif strcmp(this.Orders{1}.Status, 'Filled') || strcmp(this.Orders{2}.Status, 'Filled')
                        this.TradeStatus = 5;
                    end
                case 5 % one side exited
            end
            
        end
        
        function OnTrade( this, Trade )
            this.LiveAmount(Trade.InstrumentID) = this.LiveAmount(Trade.InstrumentID) + Trade.Amount;
            switch this.TradeStatus
                case 0 % clean | both sides exited
                case 1 % entry orders submitted
                case 2 % one side filled
                    if sum(this.LiveAmount == 0) == 0
                        this.TradeStatus = 3;
                        this.Orders = cell(2, 1);
                    elseif sum(this.LiveAmount == 0) == 2
                        this.TradeStatus = 0;
                        this.Orders = cell(2, 1);
                    end
                case 3 % both sides filled
                case 4 % exit orders submitted
                case 5 % one side exited
                    if sum(this.LiveAmount == 0) == 2
                        this.TradeStatus = 0;
                        this.Orders = cell(2, 1);
                    end
            end
        end
        
    end
    
end

