StartDate = datetime(2015, 8, 20);
EndDate = datetime(2015, 8, 20);
Contract1 = 'CU1509';
Contract2 = 'CU1510';
BeginIndex = 1;
EndIndex = 60000;

Data1 = LoadMat_SHFE_Range(Contract1, StartDate, EndDate);
Data2 = LoadMat_SHFE_Range(Contract2, StartDate, EndDate);
EndIndex = min(EndIndex, Data1.Length);
Length = EndIndex - BeginIndex;

% calculate spread
Spd = Data2.Mid(BeginIndex + 1 : EndIndex, 1) - Data1.Mid(BeginIndex + 1 : EndIndex, 1);

X0 = [ 5; 0; 5; 0 ];
LB = [ 0; -20; 0; -20 ];
UB = [ 10; 20; 10; 20 ];
OtherParams = 600;
IntCon = [];
[ X, PnL, NbTrade ] = StrategyOptimise(@CalSpdQuantileSignal, X0, OtherParams, LB, UB, IntCon, Spd, 0);

[ Signal, EntryLevels, ExitLevels ] = CalSpdQuantileSignal(Spd, X, OtherParams);
BuyPeriod = Signal > 0;
BuyPeriod = find(BuyPeriod(1 : end - 1)) + 1;
SellPeriod = Signal < 0;
SellPeriod = find(SellPeriod(1 : end - 1)) + 1;
BuySpd = Spd(BuyPeriod);
SellSpd = Spd(SellPeriod);

% plot
figure('name', 'Signals');
hold on;
plot(Spd, 'b');
plot(EntryLevels, 'g');
plot(ExitLevels, 'r');
plot(BuyPeriod, BuySpd, 'g');
plot(SellPeriod, SellSpd, 'r');
hold off;