StartDate = datetime(2015, 8, 25);
EndDate = datetime(2015, 8, 30);
Contract1 = 'CU1510';
Contract2 = 'CU1601';
Data1 = LoadMat_SHFE_Range(Contract1, StartDate, EndDate);
Data2 = LoadMat_SHFE_Range(Contract2, StartDate, EndDate);
Mid = Data2.Mid - Data1.Mid;
Mid(isnan(Mid)) = [];

NVars = 3;
LB = [ 10, 0.1, 0.1 ];
UB = [ 5000, 10, 10 ];
IntCon = 1;

[ X, FVal ] = StrategyOptimise(@CalSpdMeanVolSignal, NVars, [], LB, UB, IntCon, Mid);