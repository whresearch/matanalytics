function [ Signal ] = CalSpdMeanVolSignal( Price, Params, OtherParams )
%CALSPDMEANVOLSIGNAL Summary of this function goes here
%   Detailed explanation goes here

Lookback = Params(1);
EntryMultiplier = Params(2);
ExitMultiplier = Params(3);

Mean = indicators(Price, 'ema', Lookback);
Vol = Price - Mean;
Vol = sqrt(Vol .* Vol);
Vol = indicators(Vol, 'sma', Lookback);

Signal = zeros(length(Price), 1);
for i = 2 : length(Price)
    Z = (Price(i) - Mean(i)) / Vol(i);
    if Z > EntryMultiplier
        Signal(i) = -1;
    elseif Z < -EntryMultiplier
        Signal(i) = 1;
    elseif Signal(i - 1) < 0 && Z < -ExitMultiplier
        Signal(i) = 0;
    elseif Signal(i - 1) > 0 && Z > ExitMultiplier
        Signal(i) = 0;
    else
        Signal(i) = Signal(i - 1);
    end
end

end

