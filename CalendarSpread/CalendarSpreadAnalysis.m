StartDate = datetime(2015, 8, 20);
EndDate = datetime(2015, 8, 30);
Contract1 = 'CU1509';
Contract2 = 'CU1510';
BeginIndex = 1000;

EndIndex = 6001000;
Interval = 500;
EntryMultiplier = 2;
ExitMultiplier = 1;

Data1 = LoadMat_SHFE_Range(Contract1, StartDate, EndDate);
Data2 = LoadMat_SHFE_Range(Contract2, StartDate, EndDate);
EndIndex = min(EndIndex, Data1.Length);
Length = EndIndex - BeginIndex;
X = transpose(1 : EndIndex - BeginIndex);

Data1_Spread = Data1.Ask(BeginIndex + 1 : EndIndex, 1) - Data1.Bid(BeginIndex + 1 : EndIndex, 1);
Data2_Spread = Data2.Ask(BeginIndex + 1 : EndIndex, 1) - Data2.Bid(BeginIndex + 1 : EndIndex, 1);
% figure('name', 'Bid/Ask Spreads');
% hold on;
% plot(X, Data1_Spread);
% plot(X, Data2_Spread);

% calculate spread
Spd_Mid = Data2.Mid(BeginIndex + 1 : EndIndex, 1) - Data1.Mid(BeginIndex + 1 : EndIndex, 1);
Spd_Ask = Data2.Ask(BeginIndex + 1 : EndIndex, 1) - Data1.Bid(BeginIndex + 1 : EndIndex, 1);
Spd_Bid = Data2.Bid(BeginIndex + 1 : EndIndex, 1) - Data1.Ask(BeginIndex + 1 : EndIndex, 1);

% calculate mean & vol
Spd = Data2.Mid(BeginIndex + 1 : EndIndex, 1) - Data1.Mid(BeginIndex + 1 : EndIndex, 1);
Spd_Mean = indicators(Spd, 'ema', Interval);
Spd_Vol = Spd - Spd_Mean;
Spd_Vol = sqrt(Spd_Vol .* Spd_Vol);
Spd_Vol = indicators(Spd_Vol, 'sma', Interval);
Spd_Upper = Spd_Mean + EntryMultiplier * Spd_Vol;
Spd_Lower = Spd_Mean - EntryMultiplier * Spd_Vol;

% plot
figure('name', 'Calendar Spread');
hold on;
plot(X, Spd);
plot(X, Spd_Mean);
plot(X, Spd_Upper);
plot(X, Spd_Lower);

PnL = zeros(Length, 1);
Pos = 0;
NbTrades = 0;
for i = 1 : Length
    
    if i > 1
        PnL(i) = PnL(i - 1) + Pos * (Spd_Mid(i) - Spd_Mid(i - 1));
    end
    
    Z = (Spd(i) - Spd_Mean(i)) / Spd_Vol(i);
    
    if Pos == 0
        if Z > EntryMultiplier
            Pos = -1;
            NbTrades = NbTrades + 1;
        elseif Z < -EntryMultiplier
            Pos = 1;
            NbTrades = NbTrades + 1;
        end
    elseif Pos > 0
        if Z > ExitMultiplier
            Pos = 0;
        end
    elseif Pos < 0
        if Z < -ExitMultiplier
            Pos = 0;
        end
    end
end
fprintf('Total PnL %i \n', PnL(end));
fprintf('Number of Trades %i \n', NbTrades);
fprintf('Avg PnL Per Trade %f \n', PnL(end) / NbTrades);
fprintf('Avg Spread Contract 1 %f \n', mean(Data1_Spread));
fprintf('Avg Spread Contract 2 %f \n', mean(Data2_Spread));

% plot
figure('name', 'Profit & Loss');
plot(PnL);