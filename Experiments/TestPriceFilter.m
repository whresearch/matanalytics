% Prepare Parameters
Contract = 'cu1507';
StartDate = datetime(2015, 5, 1);
EndDate = datetime(2015, 5, 6);
MinTick = 10;

% Load data 
Data = SHFE_MatReader2(Contract, StartDate, EndDate);

% Filter Price
FBid = PriceFilter(Data.Bid, MinTick);
FAsk = PriceFilter(Data.Ask, MinTick);

figure('name', 'Filtered Bid&Ask');
hold on;
plot(FBid);
plot(FAsk);
hold off;