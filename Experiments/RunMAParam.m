% Load data 
Data = LoadMat_SHFE_Range('cu1506', datetime(2015, 5, 1), datetime(2015, 6, 1));
% Data.FilterByPrice('Mid');

% Calculate Mid-Price
Mid = (Data.Bid + Data.Ask) / 2;
Mid(isnan(Mid)) = [];
Period = 10;
Threshold = 1e-4;

PnL = zeros(length(Mid), 1);
Pos = 0;
NbTrades = 0;
for i = 1 : length(Mid)
    
    if i > 1
        PnL(i) = PnL(i - 1) + (Mid(i) - Mid(i - 1)) * Pos;
    end
    
    if i > Period && mod(i, Period) == 0
        Ret = Mid(i) / Mid(i - Period) - 1;

        if Ret < -Threshold
            NewPos = 1;
        elseif Ret > Threshold
            NewPos = -1;
        else
            NewPos = 0;
        end
        
        if NewPos ~= Pos
            if Pos ~= 0
                NbTrades = NbTrades + 1;
            end
            Pos = NewPos;
        end
        
    end
    
end

plot(PnL);