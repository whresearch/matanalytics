classdef TripMAStrategy < Strategy
    %MASTRATEGY Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        Values = NaN(500, 1);
        ShortPeriod = 10;
        MidPeriod = 40;
        LongPeriod = 100;
    end
    
    methods
        
        function [ TradeInstrumentID, Amount, Price, IsIOC ] = OnMarketDataEvent( this, LiveAmount, InstrumentID, DepthLevel, Time, Last, Bid, Ask, BidVolume, AskVolume, OpenInterest, Volume )
            
            TradeInstrumentID = InstrumentID;
            Amount = 0;
            Price = 0;
            IsIOC = true;
            
            Mid = (Bid(1) + Ask(1)) / 2;
            this.Values = [ this.Values(2:end); Mid ];
            
            if ~isnan(this.Values)
                
                SMAS = mean(this.Values(end - this.ShortPeriod + 1 : end));
                SMAM = mean(this.Values(end - this.MidPeriod + 1 : end));
                SMAL = mean(this.Values(end - this.LongPeriod + 1 : end));
                
                if this.Values(end) > SMAS && SMAS > SMAM && SMAM > SMAL && LiveAmount <= 0
                    Amount = 1 - LiveAmount;
                    Price = Mid;
                elseif this.Values(end) < SMAS && SMAS < SMAM && SMAM < SMAL && LiveAmount >= 0
                    Amount = -1 - LiveAmount;
                    Price = Mid;
                elseif this.Values(end) < SMAM && LiveAmount > 0
                    Amount = -LiveAmount;
                elseif this.Values(end) > SMAM && LiveAmount < 0
                    Amount = -LiveAmount;
                end
                
            end
        end
        
    end
    
end

