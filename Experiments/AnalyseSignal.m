function [ Return, Best, Worst ] = AnalyseSignal( Price, Signal, ForwardPeriods )
%ANALYSESIGNAL Summary of this function goes here
%   Detailed explanation goes here

if nargin < 2; error('2 arguments required at least'); end
if nargin < 3; ForwardPeriods = [ 20 60 120 240 600 ]; end

flag = abs(Signal) > 0;
dir = Signal(flag);
n = sum(flag);
idx = find(flag);

Return = NaN(n, length(ForwardPeriods));
Best = NaN(n, length(ForwardPeriods));
Worst = NaN(n, length(ForwardPeriods));

figure('name', 'Signal Chart');
hold on;
plot(Price);
LongPoint = find(Signal > 0);
scatter(LongPoint, Price(LongPoint), 'g');
ShortPoint = find(Signal < 0);
scatter(ShortPoint, Price(ShortPoint), 'r');

figure('name', 'Signal Analysis Results');
for i = 1 : length(ForwardPeriods)
    p = ForwardPeriods(i);
    j = 1;
    while j <= n
        Return(j, i) = dir(j) * (Price(min(idx(j) + p, end)) - Price(idx(j)));
        pnl = dir(j) * (Price(min(idx(j) + 1, end) : min(idx(j) + p, end)) - Price(idx(j)));
        Best(j, i) = max(pnl);
        Worst(j, i) = min(pnl);
%         while j < n && idx(j + 1) <= idx(j) + p;
%             j = j + 1;
%         end
        j = j + 1;
    end
    subplot(length(ForwardPeriods), 4, i * 4 - 3);
    plot(cumsum(Return(:, i)));
    title(['Return of Next ' num2str(p) ' Ticks']);
    subplot(length(ForwardPeriods), 4, i * 4 - 2);
    histogram(Return(:, i));
    title(['Return of Next ' num2str(p) ' Ticks']);
    subplot(length(ForwardPeriods), 4, i * 4 - 1);
    histogram(Best(:, i));
    title(['Best of Next ' num2str(p) ' Ticks']);
    subplot(length(ForwardPeriods), 4, i * 4);
    histogram(Worst(:, i));
    title(['Worst of Next ' num2str(p) ' Ticks']);
end

end

