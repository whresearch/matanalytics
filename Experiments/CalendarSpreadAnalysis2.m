StartDate = datetime(2015, 8, 20);
EndDate = datetime(2015, 8, 20);
Instrument = 'CU';
FrontMonth = datetime(2015, 10, 1);
EndMonth = datetime(2016, 4, 1);
Interval = 60;

% contract id
MainContract = [ Instrument datestr(FrontMonth, 'yymm') ];
MainData = LoadMat_SHFE_Range(MainContract, StartDate, EndDate);

% get moments
[ MainRet, MainVol, ~, ~ ] = GetMoments(MainData.Mid, Interval);

% calculate Y
Y = NaN(MainData.Length, 1);
Y(1 : end - Interval) = MainRet(Interval + 1 : end);

% calculate all calendar spreads
NbSpreads = months(FrontMonth, EndMonth);
Spreads = NaN(MainData.Length, NbSpreads);
SpreadChanges = NaN(MainData.Length, NbSpreads);
OtherRet = NaN(MainData.Length, NbSpreads);
OtherVol = NaN(MainData.Length, NbSpreads);
for i = 1 : NbSpreads
    
    % get data
    Contract = [ Instrument datestr(FrontMonth + calmonths(i), 'yymm') ];
    Data = LoadMat_SHFE_Range(Contract, StartDate, EndDate);
    
    % calculate spreads
    Spreads(:, i) = Data.Mid - MainData.Mid;
    SpreadChanges(2 : end, i) = Spreads(2 : end, i) - Spreads(1 : end - 1, i);
    
    % get moments
    [ OtherRet(:, i), OtherVol(:, i), ~, ~ ] = GetMoments(Spreads(:, i), Interval);
    
end

% show joint conditional relationships
for i = 1 : NbSpreads
    ShowJointConditional(MainRet, OtherRet(:, i), Y, true);
    Temp = MainRet .* OtherRet(:, i);
    ShowConditional(Temp, Y, true);
end

% method 1: linear regression modelling
% LMRes = [];
% for i = 1 : length(Interval)
%     LMRes = [ LMRes; fitlm([ MainRet(:, i) OtherRet(:, :, i) ], Y(:, i)) ];
% end
% Results: most contracts excluding long maturity proved to have
% significant effect on the future return of front contract, but the total
% explained power is very low with R2 < 3%.

% method 2: PCA
[Coeff, Score, Latent, TSquared, Explained, Mu] = pca(OtherRet);


% method 3: GMM

% method 4: Y = W * X + u