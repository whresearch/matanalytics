StartDate = datetime(2015, 8, 1);
EndDate = datetime(2015, 8, 31);
Contract = 'CU1510';
Data = DataMatReader(Contract, StartDate, EndDate);
Spread = Data.Ask(:, 1) - Data.Bid(:, 1);

MeanSpread = mean(Spread);

figure;
plot(Spread);

figure;
histogram(Spread, 10 : 10 : max(Spread)); 
