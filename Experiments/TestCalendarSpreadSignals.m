% Load data 
StartDate = datetime(2015, 8, 1);
EndDate = datetime(2015, 8, 30);
Contract1 = 'CU1510';
Contract2 = 'CU1602';

Lookback = 60;
HoldPeriod = 60;
Threshold1 = 15;
Threshold2 = 15;

Mode = 2; % 1 run seperately for all days, 2 run all days' data together

Date = StartDate;
AllTrades = [];
AllPnL = [];
while Date <= EndDate
    
    if Mode == 1
        Data1 = LoadMat_SHFE_Range(Contract1, Date);
        Data2 = LoadMat_SHFE_Range(Contract2, Date);
    else
        Date = EndDate;
        Data1 = LoadMat_SHFE_Range(Contract1, StartDate, EndDate);
        Data2 = LoadMat_SHFE_Range(Contract2, StartDate, EndDate);
    end
    
    if isempty(Data2)
        Date = Date + 1;
        continue;
    end
    
    % Calculate Data1.Mid & Spread
    Spread = Data2.Mid - Data1.Mid;

    % Prepare variables
    Signal = zeros(length(Data1.Mid), 1);
    
    Pos = 0;
    LastPrice = 0;
    HP = 0;
    PnL = zeros(length(Data1.Mid), 1);
    Trades = [];

    for i = 1 : length(Data1.Mid)

        if i > 1
            PnL(i) = PnL(i - 1) + (Data1.Mid(i) - Data1.Mid(i - 1)) * Pos;
            if Pos ~= 0
                HP = HP + 1;
                PL = (Data1.Mid(i) - LastPrice) * Pos;
                if HP == HoldPeriod
                    Pos = 0;
                    LastPrice = 0;
                    Trades = [ Trades; PL ];
                end
            end
        end

        if Pos == 0 && i > Lookback && mod(i, Lookback) == 0

            Ret = Data1.Mid(i) - Data1.Mid(i - Lookback);
            SpdChg = Spread(i) - Spread(i - Lookback);

            if Ret >= Threshold1 && SpdChg <= -Threshold2
                NewPos = -1;
                Signal(i) = -1;
            elseif Ret <= -Threshold1 && SpdChg >= Threshold2
                NewPos = 1;
                Signal(i) = 1;
            else
                NewPos = 0;
            end

            if NewPos ~= Pos
                if Pos ~= 0
                    Trades = [ Trades; (Data1.Mid(i) - LastPrice) * Pos ];
                end
                Pos = NewPos;
                LastPrice = Data1.Mid(i);
                HP = 0;
            end

        end

    end
    
    AllPnL = [ AllPnL PnL ];
    AllTrades = { AllTrades; Trades };
    
    Date = Date + 1;
end

figure;
plot(AllPnL);