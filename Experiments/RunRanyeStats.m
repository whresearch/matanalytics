% Prepare Parameters
Contract = 'cu1507';
StartDate = datetime(2015, 5, 1);
EndDate = datetime(2015, 5, 8);
TF = [ 10 20 60 120 240 ];
NbBins = 10;
XI = 2 + 0 * length(TF);
% TF = [ 60 120 300 ];
% NbBins = 10;
% XI = 2;

% Load data 
Data = SHFE_MatReader2(Contract, StartDate, EndDate);
% Data.FilterByPrice('Mid');

% Calculate Mid-Price
Mid = (Data.Bid + Data.Ask) / 2;
XAxis = transpose(1 : length(Mid));

% Calculate X - moments
[Ret, Vol, Skew, Kurt] = GetMoments(Mid, TF);
SR = Ret ./ Vol;

% Calculate X - highs and lows
[OIRet, OIVol, OISkew, OIKurt] = GetMoments(Data.OpenInterest, TF);
OISR = OIRet ./ OIVol;

% Calculate X - autocorrelations
AR1 = GetAutoCorrel(Mid, TF, 1, 10);

% Calculate X - highs and lows
[High, Low] = GetHighLow(Mid, TF);
Range = High - Low;

% Calculate X - order imbalance
[Imbalance, BidRatio] = GetOrderImbalance(Data.BidVolume(:, 1), Data.AskVolume(:, 1), TF);

% Calculate X - order imbalance
[OpenVol, TotVol, OpenRatio] = GetOpenVolume(Data.Volume, Data.OpenInterest, TF);

% Calculate X - Turning Points
[I, P, T] = GetTurningPoints(Mid, 1, 10, 1);

% Plot Mid Prices
figure('Name', 'Price Chart');
plot(XAxis, Mid, I, P);

% Aggregate X
X = [Ret, Vol, Skew, Kurt, SR, AR1, High, Low, Range];

% Calculate Y
Y = NaN(length(Mid), length(TF));
for i = 1 : length(TF)
    Y(1 : end - TF(i), i) = Ret(TF(i) + 1 : end, i);
end

