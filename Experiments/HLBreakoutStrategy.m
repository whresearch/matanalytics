classdef HLBreakoutStrategy < Strategy
    %MASTRATEGY Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        High = NaN(43, 1);
        Low = NaN(43, 1);
        Period = 40;
        Lookback = 2;
        Threshold = 20;
        TCount = 0;
    end
    
    methods
        
        function [ TradeInstrumentID, Amount, Price, IsIOC ] = OnMarketDataEvent( this, LiveAmount, InstrumentID, DepthLevel, Time, Last, Bid, Ask, BidVolume, AskVolume, OpenInterest, Volume )
            
            TradeInstrumentID = InstrumentID;
            Amount = 0;
            Price = 0;
            IsIOC = true;
            
            this.TCount = this.TCount + 1;
            this.High = [ this.High(2:end); Ask(1) ];
            this.Low = [ this.Low(2:end); Bid(1) ];
            
            if this.TCount > this.Period + this.Lookback
                
                H = max(this.High(end - this.Period - this.Lookback : end - this.Lookback));
                L = min(this.Low(end - this.Period - this.Lookback : end - this.Lookback));
                
                if LiveAmount > 0
                    if Ask(1) < H - this.Threshold
                        Amount = -LiveAmount;
                    end
                elseif LiveAmount < 0
                    if Bid(1) > L + this.Threshold
                        Amount = -LiveAmount;
                    end
                elseif Ask(1) >= H + this.Threshold
                    Amount = 1 - LiveAmount;
                    %Price = Bid;
                    %Price = (Bid(1) + Ask(1)) / 2;
                elseif Bid(1) <= L - this.Threshold
                    Amount = -1 - LiveAmount;
                    %Price = Bid;
                    %Price = (Bid(1) + Ask(1)) / 2;
                end
            end
        end
        
    end
    
end

