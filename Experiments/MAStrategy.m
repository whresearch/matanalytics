classdef MAStrategy < Strategy
    %MASTRATEGY Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        Values;
        ShortPeriod;
        LongPeriod;
    end
    
    methods
        
        function this = MAStrategy(ShortPeriod, LongPeriod)
            this.ShortPeriod = ShortPeriod;
            this.LongPeriod = LongPeriod;
            this.Values = NaN(1000, 1);
        end
        
        function [ TradeInstrumentID, Amount, Price, IsIOC ] = OnMarketDataEvent( this, LiveAmount, InstrumentID, DepthLevel, Time, Last, Bid, Ask, BidVolume, AskVolume, OpenInterest, Volume )
            
            TradeInstrumentID = InstrumentID;
            Amount = 0;
            Price = 0;
            IsIOC = true;
            
            Mid = (Bid(1) + Ask(1)) / 2;
            this.Values = [ this.Values(2 : end); Mid ];
            
            if ~isnan(this.Values)
                
                FP = PriceFilter(this.Values, 10);
                
                SMAS = mean(FP(end - this.ShortPeriod + 1 : end));
                SMAL = mean(FP(end - this.LongPeriod + 1 : end));
                
                if SMAS < SMAL
                    Amount = -1 - LiveAmount;
                    Price = FP(end) + 10;
                elseif SMAS > SMAL
                    Amount = 1 - LiveAmount;
                    Price = FP(end) - 10;
                end
            end
        end
        
    end
    
end

