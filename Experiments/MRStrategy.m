classdef MRStrategy < Strategy
    %MASTRATEGY Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        Values = NaN(300, 1);
        Period = 200;
        Threshold = 3e-4;
        TCount = 0;
    end
    
    methods
        
        function [ TradeInstrumentID, Amount, Price, IsIOC ] = OnMarketDataEvent( this, LiveAmount, InstrumentID, DepthLevel, Time, Last, Bid, Ask, BidVolume, AskVolume, OpenInterest, Volume )
            
            TradeInstrumentID = InstrumentID;
            Amount = 0;
            Price = 0;
            IsIOC = true;
            
            this.TCount = this.TCount + 1;
            this.Values = [ this.Values(2:end); (Bid(1) + Ask(1)) / 2 ];
            
            if this.TCount > length(this.Values) && mod(this.TCount, this.Period) == 0
                
                Ret = this.Values(end) / this.Values(end - this.Period) - 1;
                
                if Ret < -this.Threshold && LiveAmount >= 0
                    Amount = -1 - LiveAmount;
                    %Price = Ask;
                    Price = (Bid(1) + Ask(1)) / 2;
                elseif Ret > this.Threshold && LiveAmount <= 0
                    Amount = 1 - LiveAmount;
                    %Price = Bid;
                    Price = (Bid(1) + Ask(1)) / 2;
                else
                    Amount = - LiveAmount;
                end
            end
        end
        
    end
    
end

