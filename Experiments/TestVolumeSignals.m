% Load data 
Data = SHFE_MatReader2('cu1507', datetime(2015, 5, 4), datetime(2015, 5, 30));
% Data.FilterByPrice('Mid');

% Calculate Mid-Price
Mid = (Data.Bid + Data.Ask) / 2;
Signal = zeros(length(Mid), 1);
Lookback1 = 10;
Lookback2 = 5;
HoldPeriod = 400;
Threshold1 = 0.7;
Threshold2 = 0.7;
Threshold3 = nan;
SL = -50;
TP = 200;

PnL = zeros(length(Mid), 1);
Pos = 0;
LastPrice = 0;
Trades = [];
HP = 0;

for i = 1 : length(Mid)
    
    if i > 1
        PnL(i) = PnL(i - 1) + (Mid(i) - Mid(i - 1)) * Pos;
        if Pos ~= 0
            HP = HP + 1;
            PL = (Mid(i) - LastPrice) * Pos;
            if HP == HoldPeriod || PL >= TP || PL <= SL
                Pos = 0;
                LastPrice = 0;
                Trades = [ Trades; PL ];
            end
        end
    end
    
    if Pos == 0 && i > Lookback1 && mod(i, Lookback1) == 0
        
        Vol = sum(Data.Volume(i - Lookback1 + 1 : i));
        OI = Data.OpenInterest(i) - Data.OpenInterest(i - Lookback1);
        ORatio = (Vol + OI) / 2 / Vol;
        
        BO = sum(Data.BidVolume(i - Lookback1 + 1 : i));
        AO = sum(Data.AskVolume(i - Lookback1 + 1 : i));
        BRatio = BO / (BO + AO);
        
        Ret = Mid(i) / Mid(i - Lookback1) - 1;
        
        if (isnan(Threshold1) || ORatio > Threshold1) ...
        && (isnan(Threshold2) || BRatio < 1 - Threshold2) ...
        && (isnan(Threshold3) || Ret > Threshold3)
            NewPos = -1;
            Signal(i) = -1;
        elseif (isnan(Threshold1) || ORatio > Threshold1) ...
        && (isnan(Threshold2) || BRatio > Threshold2) ...
        && (isnan(Threshold3) || Ret < -Threshold3)
            NewPos = 1;
            Signal(i) = 1;
        else
            NewPos = 0;
        end
        
        if NewPos ~= Pos
            if Pos ~= 0
                Trades = [ Trades; (Mid(i) - LastPrice) * Pos ];
            end
            Pos = NewPos;
            LastPrice = Mid(i);
            HP = 0;
        end
        
    end
    
end

figure;
plot(PnL);