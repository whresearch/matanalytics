function [ FPrice ] = PriceFilter( Price, MinTick )
%PRICEFILTER Summary of this function goes here
%   Detailed explanation goes here

FPrice = NaN(length(Price), 1);
FPrice(1) = Price(1);

for i = 2 : length(Price)
   if Price(i) - FPrice(i - 1) >= 2 * MinTick
       FPrice(i) = Price(i) - MinTick;
   elseif Price(i) - FPrice(i - 1) <= -2 * MinTick
       FPrice(i) = Price(i) + MinTick;
   else
       FPrice(i) = FPrice(i - 1);
   end
end

end

