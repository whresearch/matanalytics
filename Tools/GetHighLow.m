function [ H, L ] = GetHighLow( Price, Interval )
%GETHIGHLOW Summary of this function goes here
%   Detailed explanation goes here

col = length(Interval);
row = length(Price);

H = NaN(row, col);
L = NaN(row, col);

for j = 1 : col
    for i = Interval(j) : row
        H(i, j) = max(Price(i - Interval(j) + 1 : i));
        L(i, j) = min(Price(i - Interval(j) + 1 : i));
    end
end

end

