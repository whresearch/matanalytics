function [ M, P, CM, CP ] = ShowConditional( X, Y, UseQuantile, NbBins, Threshold, OfS, ShowChart )
%ANALYSES Summary of this function goes here
%   Detailed explanation goes here

if nargin < 2; error('2 arguments required at least'); end
if nargin < 3; UseQuantile = false; end
if nargin < 4; NbBins = 10; end
if nargin < 5; Threshold = 0; end
if nargin < 6; OfS = 0.001; end
if nargin < 7; ShowChart = true; end
            
flag = ~isnan(X) & ~isnan(Y);
M = NaN(4, 1);
P = NaN(2, 1);
CM = NaN(4, NbBins);
CP = NaN(2, NbBins);
S = NaN(1, NbBins);
YTmp = Y(flag);
XTmp = X(flag);
XAxis = NaN(1, NbBins);

% Calculate Y Statistics
M(1) = mean(YTmp);
M(2) = std(YTmp);
M(3) = skewness(YTmp);
M(4) = kurtosis(YTmp);
P(2) = length(YTmp);
P(1) = sum(YTmp > Threshold) / P(2);

if ~UseQuantile
    XMin = quantile(XTmp, OfS);
    XMax = quantile(XTmp, 1 - OfS);
end

% Calculate X Statistics and conditional relationships
for i = 1 : NbBins
    if UseQuantile
        XBinMin = quantile(XTmp, (i - 1) / NbBins);
        XBinMax = quantile(XTmp, i / NbBins);
    else
        XBinMin = XMin + (XMax - XMin) / NbBins * (i - 1);
        XBinMax = XMin + (XMax - XMin) / NbBins * i;
    end
    XAxis(1, i) = (XBinMin + XBinMax) / 2;
    YBinTmp = YTmp(XTmp >= XBinMin & XTmp <= XBinMax);
    CM(1, i) = mean(YBinTmp);
    CM(2, i) = std(YBinTmp);
    CM(3, i) = skewness(YBinTmp);
    CM(4, i) = kurtosis(YBinTmp);
    CP(2, i) = length(YBinTmp);
    CP(1, i) = sum(YBinTmp > Threshold) / CP(2, i);
    S(1, i) = length(YBinTmp);
end

if ShowChart
    
    %XAxis = 1 : NbBins;
    figure('Name', 'Conditional Moments & Probabilities');
    
    % Draw conditional statistics charts
    subplot(3, 2, 1);
    plot(XAxis, CM(1, :), XAxis, ones(NbBins, 1) * M(1));
    subplot(3, 2, 2);
    plot(XAxis, CM(2, :), XAxis, ones(NbBins, 1) * M(2));
    subplot(3, 2, 3);
    plot(XAxis, CM(3, :), XAxis, ones(NbBins, 1) * M(3));
    subplot(3, 2, 4);
    plot(XAxis, CM(4, :), XAxis, ones(NbBins, 1) * M(4));
    
    % Draw conditional probability charts
    subplot(3, 2, 5);
    plot(XAxis, CP(1, :), XAxis, ones(NbBins, 1) * P(1));
    
    % Draw bin sample size charts
    subplot(3, 2, 6);
    [ XAxisC, ia ] = unique(XAxis);
    bar(XAxisC, S(ia));

end

end

