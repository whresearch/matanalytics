function [ OpenVol, TotVol, OpenRatio ] = GetOpenVolume( Volume, OpenInterest, Interval )
%GETHIGHLOW Summary of this function goes here
%   Detailed explanation goes here

col = length(Interval);
row = length(Volume);

OIChg = zeros(row, 1);
OIChg(2 : end) = OpenInterest(2 : end) - OpenInterest(1 : end - 1);

TotVol = NaN(row, col);
OIInterval = NaN(row, col);

for j = 1 : col
    for i = Interval(j) : row
        TotVol(i, j) = sum(Volume(i - Interval(j) + 1 : i));
        OIInterval(i, j) = sum(OIChg(i - Interval(j) + 1 : i));
    end
end

OpenVol = (TotVol + OIInterval) / 2;
OpenRatio = OpenVol ./ TotVol;

end

