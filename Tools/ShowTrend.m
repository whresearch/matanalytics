function ShowTrend( Price, Trend )
%SHOWPRICES Summary of this function goes here
%   Detailed explanation goes here

figure;
plot(Price);
hold on;

UpTrend = nan(length(Price), 1);
UpTrendI = Trend > 0;
UpTrend(UpTrendI) = Price(UpTrendI);
plot(UpTrend);

DnTrend = nan(length(Price), 1);
DnTrendI = Trend < 0;
DnTrend(DnTrendI) = Price(DnTrendI);
plot(DnTrend);

hold off;

end

