function [ AR ] = GetAutoCorrel( Price, Lookback, Lag, Interval )
%BY_RET Summary of this function goes here
%   Detailed explanation goes here

col = length(Lookback);
row = length(Price);

AR = NaN(row, col);

for j = 1 : col
    len = (Lookback(j) + Lag) * Interval;
    for i = len + 1 : row
        ptmp = Price(i - len : Interval : i);
        r = price2ret(ptmp);
        AR(i, j) = corr(r(1 : end - Lag), r(1 + Lag : end));
    end
end

end

