function [ I, P, T ] = GetTurningPoints( Price, Lvl, MinSpan, Eps )
%GETTURNINGPOINTS Summary of this function goes here
%   Detailed explanation goes here

% [H, L] = GetHighLow(Price, Span);


% smooth prices
% MP = zeros(floor(length(Price) / Span), 1);
% PI = transpose(0 : length(MP)) .* Span;
% PI(end) = length(MP);
% for i = 1 : length(MP)
%     MP(i) = mean(Price(PI(i) + 1 : PI(i + 1)));
% end
MP = Price;
PI = transpose(0 : length(Price));

% find local min and max points
T = zeros(length(MP), 1);
MPF = false(length(MP), 1);
LastI = 0;
LastT = 0;
for i = 2 : length(MP) - 1
    if MP(i) <= MP(i - 1) && MP(i) <= MP(i + 1)
        MPF(i) = true;
        T(i) = -1;
        if LastT == -1
            MPF(LastI) = false;
            T(LastI) = 0;
        end
        LastI = i;
        LastT = -1;
    elseif MP(i) >= MP(i - 1) && MP(i) >= MP(i + 1)
        MPF(i) = true;
        T(i) = 1;
        if LastT == 1
            MPF(LastI) = false;
            T(LastI) = 0;
        end
        LastI = i;
        LastT = 1;
    end
end
MP = MP(MPF);
PI = PI([true; MPF]);
T = T(MPF);

% filter by pattern
for k = 1 : Lvl
    MPF = true(length(MP), 1);
    for i = 1 : length(MP) - 3
        if MP(i) < MP(i + 1) && MP(i) < MP(i + 2) && ...
        MP(i) < MP(i + 3) && MP(i + 2) < MP(i + 3) && ...
        abs(MP(i + 1) - MP(i + 2)) < abs(MP(i) - MP(i + 2)) + abs(MP(i + 1) - MP(i + 3))
            MPF(i + 1) = false;
            MPF(i + 2) = false;
        elseif MP(i) > MP(i + 1) && MP(i) > MP(i + 2) && ...
        MP(i) > MP(i + 3) && MP(i + 2) > MP(i + 3) && ...
        abs(MP(i + 2) - MP(i + 1)) < abs(MP(i) - MP(i + 2)) + abs(MP(i + 1) - MP(i + 3))
            MPF(i + 1) = false;
            MPF(i + 2) = false;
        elseif abs(MP(i) - MP(i + 2)) <= Eps ...
        && abs(MP(i + 1) - MP(i + 3)) <= Eps
            MPF(i + 1) = false;
            MPF(i + 2) = false;
        end
    end
    MP = MP(MPF);
    PI = PI([true; MPF]);
    T = T(MPF);
    
    % filter by length
    MPF = true(length(MP), 1);
    for i = 2 : length(MP)
        if PI(i + 1) - PI(i) < MinSpan
            MPF(i - 1) = false;
        end
    end
    MP = MP(MPF);
    PI = PI([true; MPF]);
    T = T(MPF);
    
    MPF = true(length(MP), 1);
    for i = 2 : length(MP)
        if T(i - 1) * T(i) ~= -1
            MPF(i - 1) = false;
        end
    end
    MP = MP(MPF);
    PI = PI([true; MPF]);
    T = T(MPF);
    
end

% get results
PrevPI = PI;
for i = 1 : length(MP)
    if T(i) == 1
        [~, PI(i + 1)] = max(Price(PI(i) + 1 : PI(i + 1)));
    elseif T(i) == -1
        [~, PI(i + 1)] = min(Price(PI(i) + 1 : PI(i + 1)));
    end
    PI(i + 1) = PI(i) + PI(i + 1);
end

while ~isequal(PrevPI, PI)
    for i = 1 : length(MP) - 1
        if T(i) == 1
            [~, PI(i + 1)] = max(Price(PI(i) + 1 : PI(i + 2)));
        elseif T(i) == -1
            [~, PI(i + 1)] = min(Price(PI(i) + 1 : PI(i + 2)));
        end
        PI(i + 1) = PI(i) + PI(i + 1);
    end
    PrevPI = PI;
end

I = PI(2 : end);
P = Price(I);

end

