function [ Imbalance, BidRatio ] = GetOrderImbalance( BidVol, AskVol, Interval )
%GETHIGHLOW Summary of this function goes here
%   Detailed explanation goes here

col = length(Interval);
row = length(BidVol);

BidVolInterval = NaN(row, col);
AskVolInterval = NaN(row, col);

for j = 1 : col
    for i = Interval(j) : row
        BidVolInterval(i, j) = sum(BidVol(i - Interval(j) + 1 : i));
        AskVolInterval(i, j) = sum(AskVol(i - Interval(j) + 1 : i));
    end
end

Imbalance = BidVolInterval - AskVolInterval;
BidRatio = BidVolInterval ./ (BidVolInterval + AskVolInterval);

end

