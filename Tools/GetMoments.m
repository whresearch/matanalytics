function [ Ret, Vol, Skew, Kurt ] = GetMoments( Price, Interval )
%BY_RET Summary of this function goes here
%   Detailed explanation goes here

col = length(Interval);
row = length(Price);

rs = NaN(row, max(Interval));
rs(2 : end, 1) = Price(2 : end) - Price(1 : end - 1);
for i = 2 : max(Interval)
    rs(2 : end, i) = rs(1 : end - 1, i - 1);
end

Ret = NaN(row, col);
Vol = NaN(row, col);
Skew = NaN(row, col);
Kurt = NaN(row, col);

for i = 1 : col
    Ret(:, i) = mean(rs(:, 1 : Interval(i)), 2);
    Vol(:, i) = std(rs(:, 1 : Interval(i)), 0, 2);
    Skew(:, i) = skewness(rs(:, 1 : Interval(i)), 0, 2);
    Kurt(:, i) = kurtosis(rs(:, 1 : Interval(i)), 0, 2);
end

end

