function [ M, P, CM, CP ] = ShowJointConditional( X1, X2, Y, UseQuantile, NbBins, Threshold, OfS, ShowChart )
%ANALYSES Summary of this function goes here
%   Detailed explanation goes here

if nargin < 3; error('3 arguments required at least'); end
if nargin < 4; UseQuantile = false; end
if nargin < 5; NbBins = 10; end
if nargin < 6; Threshold = 0; end
if nargin < 7; OfS = 0.001; end
if nargin < 8; ShowChart = 1; end

flag = ~isnan(X1) & ~isnan(X2) & ~isnan(Y);
M = NaN(4, 1);
P = NaN(2, 1);
CM = NaN(4, NbBins, NbBins);
CP = NaN(2, NbBins, NbBins);
S = NaN(NbBins, NbBins);
YTmp = Y(flag);
X1Tmp = X1(flag);
X2Tmp = X2(flag);
X1Axis = NaN(1, NbBins);
X2Axis = NaN(1, NbBins);
X1BinMin = NaN(1, NbBins);
X1BinMax = NaN(1, NbBins);
X2BinMin = NaN(1, NbBins);
X2BinMax = NaN(1, NbBins);

% Calculate Y Statistics
M(1) = mean(YTmp);
M(2) = std(YTmp);
M(3) = skewness(YTmp);
M(4) = kurtosis(YTmp);
P(2) = length(YTmp);
P(1) = sum(YTmp > Threshold) / P(2);

if ~UseQuantile
    X1Min = quantile(X1Tmp, OfS);
    X1Max = quantile(X1Tmp, 1 - OfS);
    X2Min = quantile(X2Tmp, OfS);
    X2Max = quantile(X2Tmp, 1 - OfS);
end

% Calculate X Statistics and conditional relationships
for i = 1 : NbBins
    if UseQuantile
        X1BinMin(i) = quantile(X1Tmp, (i - 1) / NbBins);
        X1BinMax(i) = quantile(X1Tmp, i / NbBins);
        X2BinMin(i) = quantile(X2Tmp, (i - 1) / NbBins);
        X2BinMax(i) = quantile(X2Tmp, i / NbBins);
    else
        X1BinMin(i) = X1Min + (X1Max - X1Min) / NbBins * (i - 1);
        X1BinMax(i) = X1Min + (X1Max - X1Min) / NbBins * i;
        X2BinMin(i) = X2Min + (X2Max - X2Min) / NbBins * (i - 1);
        X2BinMax(i) = X2Min + (X2Max - X2Min) / NbBins * i;
    end
    X1Axis(1, i) = (X1BinMin(i) + X1BinMax(i)) / 2;
    X2Axis(1, i) = (X2BinMin(i) + X2BinMax(i)) / 2;
end
for i = 1 : NbBins
    for j = 1 : NbBins
        YBinTmp = YTmp(X1Tmp >= X1BinMin(i) & X1Tmp <= X1BinMax(i) & ...
            X2Tmp >= X2BinMin(j) & X2Tmp <= X2BinMax(j));
        S(i, j) = length(YBinTmp);
        CM(1, i, j) = mean(YBinTmp);
        CM(2, i, j) = std(YBinTmp);
        CM(3, i, j) = skewness(YBinTmp);
        CM(4, i, j) = kurtosis(YBinTmp);
        CP(2, i, j) = length(YBinTmp);
        CP(1, i, j) = sum(YBinTmp > Threshold) / CP(2, i, j);
    end
end

if ShowChart >= 1
    % Draw conditional statistics charts
    figure('Name', 'Conditional 1st Moment');
    surf(X1Axis, X2Axis, squeeze(CM(1, :, :)));
    xlabel('X1');
    ylabel('X2');
    zlabel('Y');
end

if ShowChart >= 2
    figure('Name', 'Conditional 2nd Moment');
    surf(X1Axis, X2Axis, squeeze(CM(2, :, :)));
    xlabel('X1');
    ylabel('X2');
    zlabel('Y');
end

if ShowChart >= 3
    figure('Name', 'Conditional 3rd Moment');
    surf(X1Axis, X2Axis, squeeze(CM(3, :, :)));
    xlabel('X1');
    ylabel('X2');
    zlabel('Y');
end

if ShowChart >= 4
    figure('Name', 'Conditional 4th Moment');
    surf(X1Axis, X2Axis, squeeze(CM(4, :, :)));
    xlabel('X1');
    ylabel('X2');
    zlabel('Y');
end
    
if ShowChart >= 5
    % Draw conditional probability charts
    figure('Name', 'Conditional Probabilities');
    surf(X1Axis, X2Axis, squeeze(CP(1, :, :)));
    xlabel('X1');
    ylabel('X2');
    zlabel('Y');
end
    
if ShowChart >= 6
    % Draw conditional probability charts
    figure('Name', 'Bin Sample Size');
    surf(X1Axis, X2Axis, S);
    xlabel('X1');
    ylabel('X2');
    zlabel('Y');
end

end

