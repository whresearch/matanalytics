classdef tfPassive < Strategy
%SampleStrategy Summary of this class goes here
%   Detailed explanation goes here
    
    properties
        Values
        ShortPeriod
        LongPeriod
        LiveAmount
        OrderHoldingPeriod
    end
    
    methods
        
        function this = tfPassive()
            this.ShortPeriod = 60;
            this.LongPeriod = 300;
            this.LiveAmount = 0;
            this.OrderHoldingPeriod = nan(1, 1);
            this.Values = NaN(this.LongPeriod, 1);
        end
        
        function OnMarketData( this, Snapshot )
            
            this.Values = [ this.Values(2:end); Snapshot.Mid(1) ];
            this.OrderHoldingPeriod = this.OrderHoldingPeriod + 1;
            
            if ~isnan(this.Values)
                
                SMAS = mean(this.Values(end - this.ShortPeriod + 1 : end));
                SMAL = mean(this.Values(end - this.LongPeriod + 1 : end));
                
                if SMAS > SMAL && isnan(this.OrderHoldingPeriod) && this.LiveAmount >= 0
                    this.PlaceOrder(Snapshot.InstrumentID, -this.LiveAmount, 0, true);
                    this.PlaceOrder(Snapshot.InstrumentID, -1, Snapshot.Ask(1), false);
                    this.OrderHoldingPeriod = 0;
                elseif SMAS < SMAL && isnan(this.OrderHoldingPeriod) && this.LiveAmount <= 0
                    this.PlaceOrder(Snapshot.InstrumentID, -this.LiveAmount, 0, true);
                    this.PlaceOrder(Snapshot.InstrumentID, 1, Snapshot.Bid(1), false);
                    this.OrderHoldingPeriod = 0;
                end
            end
        end
        
        function OnOrder( this, Order )
            if this.OrderHoldingPeriod > 5
                this.CancelOrder(Order);
                this.OrderHoldingPeriod = nan;
            end
        end
        
        function OnTrade( this, Trade )
            this.LiveAmount = this.LiveAmount + Trade.Amount;
            this.OrderHoldingPeriod = nan;
        end
        
    end
    
end

